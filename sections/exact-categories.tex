% Copyright 2022 Markus J. PFlaum, licensed under GNNU FDL v1.3 
% main author: 
%   Markus J. Pflaum 
%
\section{Exact categories}\label{sec:exact-categories}
%
In this section we will introduce  exact categories which allow
us to define the concepts of kernels, cokernels, images and coimages
crucial for doing homological algebra in a more abstract sense.
The main goal will to prove the snake lemma and the five lemma in exact
categories.
%
\begin{definition}
  Let $\category{C}$ be a category. An object of $\category{C}$ which is
  is both an initial and a terminal object is termed a \emph{zero object} of
  $\category{C}$. It is necessarily uniquely defined up to isomorphism and 
  will be denoted by $\zero_{\category{C}}$ or briefly  by $\zero$.

  For every object $A$ of $\category{C}$, the unique morphism $\zero \to A$ will be denoted
  by $\zero_{\zero\to A}$. The unique morphism $A\to\zero$ will be denoted by $\zero_{A\to\zero}$.
  If $B$ is another object, we denote the composition $\zero_{\zero\to B}  \zero_{A\to \zero}: A \to B$ by
  $\zero_{A\to B}$, $\zero_{AB}$ or just $zero$ if no confusion can arise and call it the
  \emph{zero morphism} between $A$ and $B$.
\end{definition}
 
\begin{definition} 
   Assume that $\category{C}$ is a category with zero object $\zero$ and let $f : A \to B$ be a morphism
   in $\category{C}$.
  \begin{romanlist}
  \item   By a \emph{kernel} of $f$ one understands a morphism $i:K \to A$
    such that $f i = \zero_{KB}$ and such that for every morphism $j:L \to A$ which satisfies
    $f j = \zero_{LB}$ there exists a unique morphism $l: L \to K$ making the diagram
    \begin{equation}\label{dia:kernel}
      \begin{tikzcd}
        K \arrow[r,"i"] & A \\
        L \arrow[u,"l"] \arrow[ur,"j"']
      \end{tikzcd}
    \end{equation}
    commute.
  \item
    By a \emph{cokernel} of $f$ one understands a morphism $c:B \to Q$
    such that $cf = \zero_{AQ}$ and such that for every morphism $d:B \to R$ which satisfies
    $df = \zero_{AR}$ there exists a unique morphism $r: Q \to R$ making the diagram
    \begin{equation}\label{dia:cokernel}
      \begin{tikzcd}
        B \arrow[r,"c"]  \arrow[dr,"d"'] & Q  \arrow[d,"r"]\\
        & R
      \end{tikzcd}
    \end{equation}
    commute.
  \end{romanlist}
  \end{definition}

  \begin{remark}
    By definition it is clear that the kernel of $f:A\to B$ can be identified with the equalizer
    $\equalizer (f,\zero_{AB})$ and that the cokernel of  $f:A\to B$ coincides with the coqualizer $\coequalizer (f,\zero_{AB})$.
    Either using this observation and uniqueness (up to isomorphism) of equalizers and coequalizers or by
    direct argument using the universal property of the kernel respectively the cokernel
    one shows that kernels and cokernels are unique up to unique isomorphism.
    By this uniqueness property it makes sense to give the kernel and the cokernel of $f$ each a symbol.
    We will from now on write $\ker (f) : \Ker (f) \to A$ for the kernel of $f$ and
    $\coker (f) : B \to \Coker (f)$ for the cokernel of $f$. Note that
    $\Ker (f)$ andf $\Coker (f)$ are objects of the underlying category whereas  $\ker (f) $ and $\coker (f)$ are morphisms.
  \end{remark}

  \begin{lemma}\label{thm:kernels-monic-cokernels-epic}
     In a category  $\category{C}$ with zero object $\zero$ all kernels are monomorphisms and all co\-kernels are epimorphisms.
  \end{lemma}

  \begin{proof}
    Let $f: A \to B$ be a morphism and assume that $i : K \to A$ is a kernel. Let $g,h : C \to K$ be two morphisms and assume that
    $ig = ih$. Denote that morphism by $j$. Then \[fj = fig = fih =\zero_{CB} \ .\] Hence, by the universal property of the kernel there
    exists a unique morphism $ C \to K$ making the diagram
    \begin{equation*}
      \begin{tikzcd}
        K \arrow[r,"i"] & A \\
        C \arrow[u] \arrow[ur,"j"']
      \end{tikzcd}
    \end{equation*}
    commute. But both $g$ and $h$ make this diagram commute, so they have to coincide. Therefore, $i$ is a monomorphism.
    The argument for the cokernel is dual.
  \end{proof}
  
    \para
    Still working in a category $\category{C}$ with a zero object and all kernels and cokernels
    we construct two binary relations $\succedes$ and $\precedes$ between morphisms in
    $\category{C}$ having the same codomain respectively  the same domain. 

    For morphisms of the form $u:X \to Z$ and $v:Y \to Z$ we say that $u$ \emph{succeeds} $v$,
    % or that $u$ \emph{defines a subobject succeeding} $v$,
    in signs $u \succedes v$ if  there exists a morphism $y : Y \to X$ such that the diagram
    \begin{equation*}
      \begin{tikzcd}
        Y \arrow[rd,"v"]\arrow[d,"y"']  \\
        X \arrow[r,"u"'] & Z
      \end{tikzcd}
    \end{equation*}
    commutes. In other words, $u$ to succeed $v$ means that $v$ factors through $u$.
    If  $u$ succeeds $v$ and $v$ succeeds $u$, we say that $u$ and $v$ are
    \emph{s-equivalent} and write $u \equiv_{\textup{s}} v$.
    
    Given two morphisms of the form $f:A \to B$ and $g:A \to C$ we say that
    $f$ \emph{precedes} $g$, %or  that $f$ \emph{defines a quotient object preceding} $g$,
    in signs $f  \precedes g$, if $g$ factors through $f$, that is, if there exists a morphism
    $c : B \to C$ such that the diagram
    \begin{equation*}
      \begin{tikzcd}
        & C \\
        A \arrow[ru,"g"]\arrow[r,"f"'] & B \arrow[u,"c"']
      \end{tikzcd}
    \end{equation*}
    commutes. If $f$ precedes $g$ and $g$ precedes $f$, then we call $f$ and $g$
    \emph{q-equivalent} and write $f \equiv_{\textup{q}} g$.

    By definition it is clear that the relations   $\succedes$ and $\precedes$ are reflexive and transitive.
    If $\category{C}$ is a small category,  $\succedes$ and $\precedes$ are therefore
    preorders on the morphism sets $\category{C} (-,Z)$ and $\category{C} (A,-)$, respectively.
    Since  $\succedes$ and $\precedes$ are reflexive and transitive so are s-equivalence and q-equivalence.
    Both are symmetric by definition, hence s-equivalence and q-equivalence are equivalence relations. 
    In general, if $f$ and $g$ are q-equivalent, the \emph{relating} morphisms $b$ and $c$ which fulfill
    the equalities $g = c f$ and $f= b g$ are neither uniquely determined nor need to be isomorphisms.
    An analogous observation holds for s-equivalence. The following results state conditions under which
    the relating morphisms for s-equivalence and for q-equivalence are isomorphisms.

    \begin{lemma}
      Let $\category{C}$ be a category with zero object and all kernels and cokernels.
      Assume that $A$ and $Z$ are objects of $\category{C}$.
      \begin{romanlist}
      \item\label{ite:s-equivalence-monos-equivalent-relating-isomorphisms}
        Two monomorphisms $u:X\to Z$ and $v:Y\to Z$ are s-equivalent if and only there exists an isomorphism
        $y: Y \to X $ such that $v = u y$. In this case, the inverse
        $x := y^{-1}$ satisfies $u =v x$. 
      \item
        Two epimorphisms  $f:A \to B$ and $g:A\to C$ are q-equivalent if and only if there exists an isomorphism
        $c: B \to C$ such that $g = c f$. In this case, the inverse
        $b := c^{-1}$ satisfies $f = b g$. 
      \end{romanlist}
    \end{lemma}
    \begin{proof}
      \begin{adromanlist}
      \item  Assume that $u:X\to Z$ and $v:Y\to Z$ are s-equivalent. Then there exist
        morphisms $y: Y \to X $ and $x : X \to Y$ such that $v = u y$
        and $u =v y$. Therefore,
        \[ u \, y \, x = u \, \id_X\qquad\text{and}
           \qquad v \, x \, y = v \, \id_Y \ . \]
        Since both $u$ and $v$ are monomorphisms, the equalities
        $ y x = \id_X$ and $ x y = \id_Y $ follow, hence
        $x$ and $y$ are mutually inverse isomorphisms. The converse is clear
        by definition of s-equivalence.
      \item
        The argument is dual to the one for
        \ref{ite:s-equivalence-monos-equivalent-relating-isomorphisms}.
      \end{adromanlist}
    \end{proof}

    \begin{remark}
      Given two monomorphisms  $u:X\to Z$ and $v:Y\to Z$, we will often just write
      $u \equiv v$ instead of $u \equiv_{\textup{s}} v$ to denote that $u$ and $v$ are s-equivalent.
      Similarly, for two epimorphisms  $f:A \to B$ and $g:A\to C$ we usually abbreviate q-equivalence,
      that is $f \equiv_{\textup{q}} g$, 
      by $f \equiv g$.
      In addition, we will often briefly say that $u$ and $v$ (respectively $f$ and $g$) are \emph{equivalent}
      instead of saying they are s-equivalent (respectively q-equivalent).
      By the preceding lemma, these agreements will not cause any confusion rather will  
      they improve readability. 
    \end{remark}
    
  \begin{definition}
  Assume that $\category{C}$ is a category with zero object $\zero$ and that all morphisms in $\category{C}$ have a
  kernel and a cokernel. Let $f:A\to B$ be a morphism. 
  \begin{romanlist}
  \item The kernel $\ker (\coker (f)): \Ker(\coker(f)) \to B$ of the cokernel
    of $f$ is called the \emph{image} of $f$. It is denoted by $\img (f): \Img(f) \to B$. 
  \item
    The cokernel $\coker(\ker (f)): A\to \Coker(\ker (f))$
    of the kernel of $f$ is called the \emph{coimage} of $f$.
    One denotes it by $\coim (f): A \to \Coim (f)$. 
  \end{romanlist}
  \end{definition}

  \begin{lemma}
    In a category  $\category{C}$ which possess a zero object $\zero$ and all kernels and cokernels,
    the follow natural equivalences hold true.
    \begin{romanlist}
    \item
      \[ \ker \coker \ker f \equiv \ker f \]
    \item
      \add{$\coker ker \coker$ case!}
    \end{romanlist}
    \question{does one need $\category{C}$ to be exact?}
  \end{lemma}
  
  \para
  Under the same assumptions as before that $\category{C}$ has a zero object and possesses all kernels and cokernels
  consider in the following diagram a morphism $f:A\to B$ and the associated kernel, coimage, image and cokernel of $f$: 
  \begin{equation}\label{diag:natural-decomposition-morphism-exact-category}
      \begin{tikzcd}
        & A \arrow[rrr,"f"]\arrow[rd,"\coim (f)"']  &&& B \arrow[rd,"\coker (f)"] & \\
        \Ker (f) \arrow[ru,"\ker (f)"] & & \Coim (f)\arrow[urr,"f^\prime",dashed]
        \arrow[r,"\overline{f}"',dotted] & \Img (f)\arrow[ru,"\img (f)"'] && \Coker (f)
      \end{tikzcd}
    \end{equation}
    Since the composition $f \comp \ker (f)$ coincides with the zero  morphism, there exists by the universal
    property of the coimage a unique morphism $f^\prime:\Coim (f) \to B$ such that the diagram consisting of the
    straight and dashed arrows commutes. Since the composition
    \[  \coker (f) \comp f^\prime \comp \coim (f) = \coker (f)\comp f \]
    is the zero morphism  and $\coim (f)$ is an epimorphism by \Cref{thm:kernels-monic-cokernels-epic},
    the morphism $  \coker (f) \comp f^\prime$ has to be the zero morphism as well.
    By the universal property of the image, there exists a unique morphism $\overline{f}: \Coim (f) \to \Img (f)$
    making the full diagram commutate. 
    
    \begin{definition}
      A category $\category{C}$ which has a zero object $\zero$ and possesses all kernels and cokernels is called
      an \emph{exact category} if for all morphisms $f:A\to B$ the associated unique  morphism
      $\overline{f}: \Coim (f) \to \Img (f)$ making the diagram
      \eqref{diag:natural-decomposition-morphism-exact-category} commute is an isomorphism.
    \end{definition}
    
    
    
    \begin{definition}
      Let
      \[
        f_\bullet : \qquad
        \ldots A_{n-1} \overset{f_{n-1}}{\longrightarrow} A_{n}  \overset{f_{n}}{\longrightarrow} A_{n+1} \ldots 
      \]
      be a sequence of objects and morphisms in the exact category $\category{C}$. One says that  $f_\bullet$ is
      \emph{exact} at $A_n$ if the kernel of $f_{n}$ is an image of $f_{n-1}$. If the sequence is exact at
      each $A_n$, then $f_\bullet$ is called an \emph{exact sequence}. 
    \end{definition}

    From now on we always assume that the underlying category $\category{C}$ is exact. 
    \begin{lemma}
      Let
      \begin{equation}
      \begin{tikzcd}  
          A \arrow[r,"f"] & B \arrow[r,"g"] &  C
      \end{tikzcd}
      \end{equation}
      be an exact sequence of objects and morphisms in $\category{C}$.  Then the following holds true.
      \begin{romanlist}
      \item
        If $d : D \to B$ is a morphism such that the diagram of straight arrows in the diagram
        \begin{equation}
          \label{eq:lifting-morphism-left-short-exact-sequence-exact-category}
          \begin{tikzcd}
            &&D\arrow[dl,dashed]\arrow[d]\arrow[rd,"\zero"]&\\
            \zero \arrow[r]&A\arrow[r] &B \arrow[r] & C
          \end{tikzcd}
        \end{equation}
        commutes and such that the horizontal sequence is exact, then there exists a unique morphism
        $D\to A $ making the full diagram commute.
      \item
        If $e: B \to E$ is a morphism such that the diagram of straight arrows in the diagram
        \begin{equation}
          \label{eq:lifting-morphism-right-short-exact-sequence-exact-category}
          \begin{tikzcd}
            A\arrow[r]\arrow[dr,"\zero"'] &B \arrow[r]\arrow[d] & C \arrow[dl,dashed] \arrow[r] & \zero \\
            &E&&
          \end{tikzcd}
        \end{equation}
        commutes and such that the horizontal sequence is exact, then there exists a unique morphism
        $C\to E $ making the full diagram commute.
      \end{romanlist}
    \end{lemma}
    \begin{proof}
      \begin{adromanlist}
      \item
        Since $gd = \zero$, there exists a unique morphism $\overline{d} : D \to \Ker g$ such that
        $d = \ker d \comp \overline{d}$. Since by exactness $\ker g \equiv \img f$, we actually have
        unique morphism $\overline{d}: D \to \Img f$ such that  $d = \img f \comp \overline{d}$. 
      \end{adromanlist}
    \end{proof}
    
    \begin{lemma}
      The sequence
      \[
        f_\bullet : \qquad
        \ldots A_{n-1} \overset{f_{n-1}}{\longrightarrow} A_{n}  \overset{f_{n}}{\longrightarrow} A_{n+1} \ldots 
      \]
      in $\category{C}$ is exact at $A_n$ if and only the following conditions hold true:
      \begin{romanlist}
      \item  $f_{n} \comp f_{n-1} = \zero_{A_{n-1} \to  A_{n+1}}$.
      \item  $\coker (f_{n-1}) \comp \ker (f_{n}) = \zero_{\Ker(f_{n})\to \Coker f_{n-1}}$.
      \end{romanlist}
    \end{lemma}


   