% Copyright 2016-2021 Frederic Latremoliere, licensed under GNU FDL v1.3 
% main author: 
%   Frederic Latremoliere
% modifications and contributions by: 
%   Markus J. Pflaum 
%
\section{The category of topological spaces}
\label{sec:category-topological-spaces}

\subsection*{Topologies and continuous maps}
\begin{definition}
Let $X$ be a set. By a topology on $X$ on understands  a set  $\topology$ of 
subsets of $X$ such that:
\begin{axiomlist}[Top]
\setcounter{enumi}{-1}
\item\label{axiom:openness-full-empty-set-open}
      The sets  $X$ and $\emptyset$ are both elements of $\topology$.
\item\label{axiom:openness-union-open-sets}
      The union of any collection of elements of $\topology$ is again in $\topology$ that means if
      $(U_i)_{i\in I}$ is a family of elements $U_i\in \topology$, then
      $\bigcup_{i\in I} U_i \in \topology$.
\item \label{axiom:openness-finite-intersection-open-sets}
      The intersection of finitely many elements of $\topology$ is again in $\topology$ that means 
      for every natural $n$ and $U_1, \ldots , U_n  \in \topology$ one has 
      $ \bigcap_{i = 1}^n U_i \in \topology$.
\end{axiomlist}

A pair $(X,\topology)$ is a called a \emph{topological space} when $X$ is a 
set and $\topology$ a topology on $X$. Moreover, a subset $U$ of $X$ is 
called \emph{open} if $U\in \topology$ and \emph{closed} if 
$\complement_X U \in \topology$.
\end{definition}

\begin{remarks}
  \begin{environmentlist}
  \item
  Strictly speaking, Axiom \ref{axiom:openness-full-empty-set-open} can be derived from 
  Axioms \ref{axiom:openness-union-open-sets} and \ref{axiom:openness-finite-intersection-open-sets},
  since the union of an empty family of subsets of $X$ coincides with $\emptyset$, and the 
  intersection of an empty family of subsets of $X$ coincides with $X$. Nevertheless, it is 
  useful to require it, since in proofs one often shows Axiom \ref{axiom:openness-union-open-sets}
  only for non-empty families of open sets, and 
  Axiom \ref{axiom:openness-finite-intersection-open-sets} only for the case of the intersection
  of two open subsets. Then it is necessary to verify Axiom \ref{axiom:openness-full-empty-set-open},
  too, when one wants to prove that a given set of subsets of $X$ is a topology.     
  \item
    When using the notation  $\topology_X$ for a topology we always mean that  $\topology_X$ is a
    topology on the space $X$.
  \end{environmentlist}
\end{remarks}

\begin{examples}
\label{ex:examples-topological-spaces}
\begin{environmentlist}
\item
  For every set $X$ the power set $\powerset{X}$ is a topology on $X$. It is 
  called the \emph{discrete} or \emph{strongest} topology on $X$.
\item 
  The set $\big\{ \emptyset , X \big\}$  is another topology on a set $X$
  called the   \emph{indiscrete} or \emph{trivial} or 
  \emph{weakest} topology on $X$. Unless $X$ is empty or has only one element,
  the discrete and indiscrete topologies differ.
\item
  Let $S$ be a set $\{ 0, 1\}$.
  Then the set $\big\{  \emptyset , \{ 1 \}, \{ 0, 1 \} \big\} $ is a topology 
  on $S$ which  does neither coincide with the discrete nor the 
  indiscrete topology. The set $S$ with this topology is called 
  \emph{Sierpi\'{n}ski space}. The closed sets of the Sierpi\'{n}ski space
  are $\emptyset$, $\{ 0 \}$ and $S$.
\item\label{ex:standard-topology-reals}
  The \emph{standard topology} on the set  of real numbers $\R$ consists of 
  all subsets $U\subset \R$ such that for each $x\in U$ there are real numbers
  $a,b$ satisfying $a < x < b$  and $ \openint{a,b} \subset U$. The standard topology on $\R$ 
  will be denoted by $\topology_{\R}$.

  Let us show that  $\topology_{\R}$ is a topology on $\R$ indeed.
  Obviously $\emptyset$ and $\R$ are elements of $\topology_{\R}$. Let 
  $U,V  \in \topology_{\R}$ and $x \in U\cap V$. 
  Then there are $a,b,c,d \in \R$ such that 
  $x \in \openint{a,b}  \subset U$ and $ x\in \openint{c,d}  \subset V$.
  Put $e := \max \{ a,c\}$ and $f := \min \{ b,d\}$.
  Then $x \in \openint{e,f}  \subset U \cap V$, which proves 
  $U \cap V \in \topology_{\R}$. If $(U_i)_{i\in I}$ is a family of
  elements $U_i \in \topology_{\R}$ and $x \in \bigcup_{i\in I} U_i$, then there exists an 
  $j \in I$ with $x \in U_j$. Choose $a,b \in \R$ such that 
  $x \in \openint{a,b}  \subset U_j$. Then 
  $x \in \openint{a,b} \subset  \bigcup_{i\in I} U_i$, which proves
  $\bigcup_{i\in I} U_i \in \topology_{\R}$. If not mentioned differently,
  we always assume the  set of real numbers to be equipped with the standard topology. 
  The standard topology coincides with the metric topology induced by the euclidean metric 
  on $\R$, see \ref{}. One therefore often calls $\topology_{\R}$  the \emph{euclidean topology} 
  on $\R$. We will use these terms interchangeably. 
\item 
  The \emph{standard topology}  $\topology_{\Q}$ 
  on the set of rational numbers  $\Q$ is defined analogously.
  It consists of all subset $U\subset \Q$ such that for each 
  $x\in U$ there exist rational numbers $a,b$ with  $a < x < b$  and 
  $ \openint{a,b} \subset U$. Like for the reals one proves that  $\topology_{\Q}$ 
  is a topology on $\Q$. Unless mentioned differently it is always assumed that 
  $\Q$  comes equipped with the standard topology. Like for $\R$, the standard topology on $\Q$
  coincides with the \emph{euclidean topology} on $\Q$ which is the one induced by the euclidean metric.  
\item 
  Let $X$ be a set, and let $\topology_\textup{cof}$ denote the set of all
  subset of $X$ which are either empty or have  finite complement in $X$. 
  Then $\topology_\textup{cof}$ is a topology on $X$ called the 
  \emph{cofinite topology}. 
\item 
  Let $X$ be a set, and let $\topology_\textup{coc}$ denote the set of all
  subset of $X$ which are either empty or have  countable complement in $X$. 
  Then $\topology_\textup{coc}$ is a topology on $X$ called the 
  \emph{cocountable topology}. 
\item
  Let $X$ be a (nonempty) set, $(Y,\topology)$ be a topological space, and 
  $f:X\rightarrow Y$ a function. Define
  \[
    f^* \topology := f^{-1} \topology := \{ f^{-1}(U) \in \power{X} \mid U \in \topology \} \ . 
  \]
  Then $(X,f^*\topology)$ is a topological space. One calls $f^*\topology$ the 
  \emph{initial topology on $X$ with respect to} $f$ or the \emph{topology on $X$ induced by} $f$.
  
  Let us verify that $f^*\topology$ is a topology on $X$ indeed. By
  $f^{-1}(Y)=X$ and $f^{-1}(\emptyset)=\emptyset$ the sets $X$ and $\emptyset$ are in 
  $f^*\topology$. Now let $(V_i)_{i\in I}$ be a family of elements of $f^*\topology$. 
  In other words we have, for each $i\in I$, $V_i = f^{-1} (U_i)$ for some 
  $U_i\in \topology$. Then $U := \bigcup_{i\in I} U_i \in \topology$ and 
  \[
    \bigcup_{i\in I}V_i =\bigcup_{i\in I}f^{-1} (U_i) = 
    f^{-1} \Big( \bigcup_{i\in I} U_i \Big) = f^{-1} (U) \in f^*\topology \ .
  \]
  Finally, let $V_1,\ldots,V_n \in f^{-1}\topology$. Then, by definition, there exist 
  $U_1, \ldots , U_n \in\topology$ such that $V_i=f^{-1}(U_i)$ for $i=1,\ldots , n$. Thus
  $U :=\bigcap_{i=1}^n U_i \in \topology$ and  
  \[
   \bigcap_{i=1}^n V_i = \bigcap_{i=1}^n f^{-1}(U_i) = 
   f^{-1} \Big( \bigcap_{i=1}^n U_i \Big) = f^{-1} (U) \in f^*\topology \ .
  \]
\item
  Let $(X,\topology)$ be a topological space, $Y$ a (nonempty) set, and 
  $g:X\rightarrow Y$ a function. Define $g_* \topology \subset \power{Y}$ as the 
  set of all $U \subset Y$ such that $g^{-1} (U)\in \topology$. Then $g_* \topology$ 
  is a topology on $Y$. It is called the \emph{final topology on $Y$ with respect to} $g$
  or the \emph{topology on $Y$ induced by} $g$. If $g :X \to Y$ is a \emph{quotient map} 
  that means that $g$ is surjective, then the final topology on $Y$ induced by 
  $g$ is also called the \emph{quotient topology on $X$ induced by} $g$. 

  Let us show why $g_* \topology$ is a topology on $Y$.  Obviously, $Y,\emptyset \in g_* \topology$.
  Let $(U_i)_{i\in I}$ be a family of elements of $g_* \topology$. Then 
  $g^{-1} (U_i) \in \topology$    for all $i\in I$ which entails 
  \[
    g^{-1} \Big( \bigcup\limits_{i\in I} U_i \Big)  = \bigcup\limits_{i\in I} g^{-1} (U_i) \in \topology  ,
  \]
  hence $\bigcup\limits_{i\in I} U_i \in g_* \topology$. If $U_1, \ldots U_k \in g_* \topology$, then 
  \[ 
    g^{-1} (U_1\cap \ldots \cap U_k) = \bigcap_{i=1}^k g^{-1} (U_i)  \in \topology .
  \]
  So $U_1\cap \ldots \cap U_k  \in g_* \topology$ and the claim is proved.  
\end{environmentlist}
\end{examples}

\para
\Cref{sec:fundamental-examples-topologies} on fundamental 
examples collects several more examples of topologies. 
For now, we will work out a few basic properties 
of topologies and their structure preserving morphisms, the continuous maps 
defined below.

\begin{definition}
Let $(X,\topology_X)$ and $(Y,\topology_Y)$ be two topological spaces
and assume that $f:X\rightarrow Y$ is a function. One says that $f$ is 
\emph{continuous} if for all $U \in \topology_Y$ the preimage $f^{-1}(U)$ 
is open in $X$. The map $f$ is called \emph{open} if $f(V)$ is open in $Y$ 
for all $V \in\topology_X$.  
\end{definition}

\begin{example}
  Any constant function $c : X \to Y$ between two topological spaces is continuous 
  since the preimage of an open set in $Y$ is either the full set $X$ or empty 
  depending on whether the image of $c$ is contained in the open set or not.  
\end{example}

\begin{thmanddef}\label{thm:category-topological-spaces}
\begin{environmentlist}
\item   
The identity map $\id_X$ on a topological space $(X,\topology_X)$ is continuous and open. 
\item
Let $(X,\topology_X)$, $(Y,\topology_Y)$ and $(Z,\topology_Z)$ be three topological 
spaces. Assume that $f:X\rightarrow Y$ and $g:Y\rightarrow Z$ are maps. 
If $f$ and $g$ are both continuous, so is $g\circ f$. If $f$ and $g$ are both open,
then  $g\circ f$ is open as well. 
\item 
Topological spaces as objects together with continuous maps as morphisms form a  category.
It is called the \emph{category of topological spaces} and will be denoted by $\cat{Top}$.  
\end{environmentlist}
\end{thmanddef}

\begin{proof}
It is obvious by definition that the identity map $\id_X$ is continuous and open. 
Now assume that  $f$ and $g$ are continuous and let $U\in\topology_Z$. 
Then $g^{-1}(U)\in\topology_Y$ by continuity of $g$. Hence 
$f^{-1}(g^{-1}(U))\in \topology_X$ by continuity of $f$. So $g\circ f$ is continuous.
If  $f$ and $g$ are open maps, and  $V\in\topology_X$, then 
$f(V) \in \topology_Y$ and $g\circ f (V) = g (f(V)) \in  \topology_Z$. Hence
the composition of two open maps is open, too. The rest of the claim follows immediately. 
\end{proof}


\subsection*{Comparison of topologies}

\para 
The initial topology $f^* \topology_Y$ induced by a function $f:X\rightarrow Y$ between topological spaces 
is a subset of the topology on $X$ if and only if $f$ is continuous. This motivates the following definition.

\begin{definition}
Let $X$ be a set. Let $\topology_1$ and $\topology_2$ be two topologies on $X$. One says that $\topology_1$ is 
\emph{finer} or \emph{stronger} than $\topology_2$ and $\topology_2$ is \emph{coarser} or \emph{weaker} than $\topology_1$ when 
$\topology_2\subset \topology_1$.
\end{definition}

\para 
Of course, inclusion induces an order relation on topologies on a given set. A remarkable property is that any 
nonempty subset of the ordered set of topologies on a given set always admits a greatest lower bound.

\begin{theorem}
\label{thm:topology-generated-set-topologies}
Let $X$ be a set. Let $\mathfrak{S}$ be a nonempty set of topologies on $E$. 
Then the set
\[
  \topology_\mathfrak{S} := \bigcap_{\topology \in \mathfrak{S}} \topology = 
  \big\{ U \in \powerset{X} \mid U \in \topology \text{ for all  }
  \topology \in \mathfrak{S} \big\}
\]
is a topology on $X$ and it is the greatest lower bound of $\mathfrak{S}$, where the order between topologies 
is given by inclusion. In other words, $\topology_\mathfrak{S}$ is the finest topology contained in each topology from 
$\mathfrak{S}$.
\end{theorem}

\begin{proof}
We first show that $\topology_\mathfrak{S}$ is a topology. Since each $\topology \in \mathfrak{S}$ is a topology on $X$, 
we have $\emptyset,X \in \topology$ for all $\topology \in \mathfrak{S}$. Hence $\emptyset,X \in \topology_\mathfrak{S}$.

Let $(U_i)_{i\in I}$ be a nonempty family of elements $U_i \in \topology_\mathfrak{S}$. Let $\topology \in \mathfrak{S}$ be arbitrary. By definition 
of $\topology_\mathfrak{S}$, we have $U_i \in \topology$ for all $i\in I$. Since $\topology$ is a topology, 
$\bigcup_{i \in I} U_i \in \topology$. Hence, as $\topology$ was arbitrary, $\bigcup_{i\in I} U_i \in \topology_\mathfrak{S}$.

Now, let $U_1,\ldots , U_n \in \topology_\mathfrak{S}$. Let $\topology \in \mathfrak{S}$ be arbitrary. By definition of $\topology_\mathfrak{S}$, 
we have $U_1, \ldots , U_n \in \topology$. Therefore, $U_1 \cap \ldots \cap U_n \in \topology$ since $\topology$ is a topology. 
Since $\topology$ was arbitrary in $\mathfrak{S}$, we conclude that $U_1 \cap \ldots \cap U_n \in \topology_\mathfrak{S}$ by definition.
 
So $\topology_\mathfrak{S}$ is a topology on $X$. By construction, $\topology_\mathfrak{S} \subset \topology$ for all $\topology \in \mathfrak{S}$, 
so $\topology_\mathfrak{S}$ is a lower bound for $\mathfrak{S}$. Assume given a new topology $\mathcal{Q}$ on $X$ such that 
$\mathcal{Q} \subset \topology$ for all $\topology \in\mathfrak{S}$. Let $U \in \mathcal{Q}$. Then we have $U \in \topology$ 
for all $\topology \in \mathfrak{S}$. Hence by definition $U\in \topology_\mathfrak{S}$. So $\mathcal{Q} \subset \topology_\mathfrak{S}$ 
and thus $\topology_\mathfrak{S}$ is the greatest lower bound of $\mathfrak{S}$.
\end{proof}

\begin{corollary}
Let $X$  be a  set, $(Y,\topology)$ be a topological space, and $f:X\to Y$ a map. 
The coarsest topology on $X$ which makes $f$ continuous is the initial topology $f^*\topology$.
\end{corollary}

\begin{proof}
Let $\mathfrak{S}$ be the set of all topologies on $X$ such that $f$ is continuous. By definition, $f^*\topology$ is a lower bound of 
$\mathfrak{S}$. Moreover, $f^* \topology \in \mathfrak{S}$. Hence $f^*\topology$ is the coarsest topology making the function 
$f:X\rightarrow Y$ continuous.
\end{proof}

\begin{proposition}
Let $(X,\topology)$  be a topological space, $Y$ a set, and $g:X\to Y$ a map. 
The finest topology on $Y$ which makes $g$ continuous is the final topology $g_*\topology$.
\end{proposition}

\begin{proof}
Let $\mathscr{S}$ be a topology on $Y$ so that $g:(X,\topology)\to(Y,\mathscr{S})$ is continuous. 
Let $U\in \mathscr{S}$. Then $g^{-1} (U)\in \topology$ by continuity of  $g:(X,\topology)\to(Y,\mathscr{S})$.
Hence  $U\in g_*\topology$ by definition, and $\mathscr{S} \subset \topology$. 
Since $g:(X,\topology)\to (Y,g_*\topology)$ is continuous by definition, the claim follows. 
\end{proof}
\para 
We can use \Cref{thm:topology-generated-set-topologies} to define other interesting topologies. Note that trivially $\powerset{X}$ is 
a topology on a given set $X$, so given any $\mathscr{S}\subset \powerset{X}$ there is at least one topology containing $\mathscr{S}$. 
From this:

\begin{propanddef}
Let $X$ be a set, and $\mathscr{S}$ a subset of $\powerset{X}$. The greatest lower bound 
of the set
\[ 
  \mathfrak{S} = \{ \topology \in \powerset{\powerset{X}}\mid  \topology
  \textrm{ is a topology on } X \: \& \: \mathscr{S} \subset \topology \} 
\]
is the coarsest topology on $X$ containing $\mathscr{S}$. We call it the 
\emph{topology generated by $\mathscr{S}$ on $X$} and denote it by $\topology_\mathscr{S}$. The topology 
$\topology_\mathscr{S}$ consists of unions of finite intersections of elements of $\mathscr{S}$ that means 
\[ 
  \topology_\mathscr{S} = \Big\{  U \in \powerset{X} \mid \exists J \, \forall j\in J \, \exists n_j\in \N \, 
  \exists U_{j,1},\ldots ,U_{j,n_j} \in \mathscr{S} : \:  U = \bigcup_{j\in J} \bigcap_{k=1}^{n_j} U_{j,k} \Big\} \ .
\]
\end{propanddef}

\begin{proof}
   By definition of $\mathfrak{S}$ and \Cref{thm:topology-generated-set-topologies},
   $\topology_\mathfrak{S} = \bigcap_{\topology \in \mathfrak{S}} \topology $ is a 
   topology on $X$ which contains $\mathscr{S}$. Hence $\topology_\mathfrak{S}$ is an element of $\mathfrak{S}$ 
   and a subset of any element of  $\mathfrak{S}$. The first claim follows. 
   To verify the second, observe that it suffices to show that 
   \[ 
     \mathscr{R} := \Big\{  U \in \powerset{X} \mid \exists J \, \forall j\in J \, \exists n_j\in \N \, 
  \exists U_{j,1},\ldots ,U_{j,n_j} \in \mathscr{S} : \:  U = \bigcup_{j\in J} \bigcap_{k=1}^{n_j} U_{j,k} \Big\}
   \]
   is a topology. The set $\mathscr{R}$ being a topology namely entails $ \topology_\mathscr{S}  \subset \mathscr{R} $
   because $ \mathscr{S} \subset \mathscr{R}$.  
   The inclusion $\mathscr{R} \subset  \topology_\mathscr{S}$ is clear by definition, since $\topology_\mathscr{S}$ is a 
   topology containing $\mathscr{S}$.
   So let us show that $\mathscr{R}$ is a topology. Obviously $\emptyset$ and $X$ are elements of $\mathscr{R}$ 
   because $\bigcup_{i\in \emptyset} U_i = \emptyset$ and $\bigcap_{k=1}^0 U_k = X$. 
   Now assume that $(U_i)_{i\in I}$ is a family of elements of $\mathscr{R}$. Then there exists for each $i\in I$ a set $J_i$ and
   for every $j\in J_i$ a natural number $n_{i,j}$ together with elements $U_{i,j,1}, \ldots , U_{i,j,n_{i,j}} \in \mathscr{S}$
   such that  
   \[
    U_i = \bigcup_{j\in J_i} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k} \ .
   \]
   Put $J := \bigcup_{i\in I} \{ i \} \times J_i$. Then
   \[
    U :=  \bigcup_{i\in I} U_i = \bigcup_{i\in I} \bigcup_{j\in J_i} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k}
      =  \bigcup_{(i,j) \in J} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k} \in \mathscr{R} \ .
   \]
   Last assume $U_1, \ldots U_n \in \topology$ where $n\in \N$.  Then one can find for each $i \in \{ 1,\ldots, n \}$ a set $J_i$ and
   for every $j\in J_i$ a natural number $n_{i,j}$ together with elements $U_{i,j,1}, \ldots , U_{i,j,n_{i,j}} \in \mathscr{S}$
   such that  
   \[
    U_i = \bigcup_{j\in J_i} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k} \ .
   \]
   Put $J := J_1 \times \ldots \times J_n$. Then
   \[
     U := \bigcap_{i=1}^n U_i =  \bigcap_{i=1}^n \bigcup_{j\in J_i} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k} = 
     \bigcup_{(j_1,\ldots ,j_n)\in J} \: \bigcap_{k_1=1}^{n_{1,j_1}} U_{1,j_1,k_1} \cap \ldots \cap  \bigcap_{k_n=1}^{n_{n,j_n}} U_{n,j_n,k_n} 
     \in \mathscr{R} \ .  
   \]     
   Hence $\mathscr{R}$ is a topology, indeed, and the proposition is proved. 
\end{proof}

\begin{definition}
   Let $X$ be a set, and $\topology$ a topology on $X$. One calls a subset 
   $\mathscr{S} \subset \topology$ a \emph{subbase} (or \emph{subbasis}) of the 
   topology if $\topology$ coincides with  $\topology_\mathscr{S}$. 
   If in addition $X = \bigcup_{S\in \mathscr{S}} S$, the subbase $\mathscr{S}$
   is said to be \emph{adequate}. 
\end{definition}

\subsection*{Bases of topologies}
\para
When inducing a topology from a family $\mathscr{B}$ of subsets of some set $X$, the fact that $\mathscr{B}$ enjoys the following 
property greatly simplifies the description of the topology $\topology_\mathscr{B}$ generated by $\mathscr{B}$.

\begin{definition}
Let $X$ be a set. A \emph{base} (or \emph{basis}) on $X$ is a subset $\mathscr{B}$ of the powerset $\powerset{X}$ such that
\begin{axiomlist}[Bas]
\item $X = \bigcup_{B \in \mathscr{B}}B $,
\item For all $B_1,B_2 \in \mathscr{B}$ and all $ x \in B_1 \cap B_2$ there exists 
      a $B \in \mathscr{B}$ such that $x \in B$ and $B \subset B_1 \cap B_2$.
\end{axiomlist} 
\end{definition}

The main purpose for this definition stems from the following theorem:

\begin{theorem}
Let $X$ be some set. Let $\mathscr{B}$ be a base on $X$. Then the topology generated by $\mathscr{B}$ coincides
with the set of unions of elements of $\mathscr{B}$ that means 
\[
  \topology_\mathscr{B} = \left\{ \bigcup_{B \in \mathscr{U}} B \in \power{\power{X}} \Bigmid \mathscr{U} \subset \mathscr{B} \right\} \ .
\]
\end{theorem}

\begin{proof}
Denote, for this proof, the set $\big\{ \bigcup_{B \in \mathscr{U}} B \bigmid \mathscr{U} \subset \mathscr{B} \big\}$ by $\mathscr{S}$
and let us abbreviate $\topology_\mathscr{B}$ by $\topology$. We wish to prove that $\topology = \mathscr{S}$.
First, note that $\mathscr{B}\subset \mathscr{S}$ by construction. By definition, $\mathscr{B} \subset \topology$. 
Since $\topology$ is a topology, it is closed under arbitrary unions. Hence $\mathscr{S} \subset \topology$.
To prove the converse, it is sufficient to show that $\mathscr{S}$ is a topology. As it contains $\mathscr{B}$, and $\topology$ 
is the smallest such topology, this will provide us with the inverse inclusion.
By definition, $\bigcup_{B\in \emptyset}B = \emptyset$ and thus $\emptyset \in \mathscr{S}$. By assumption, since $\mathcal{B}$ is a base, 
$X = \bigcup_{B\in \mathscr{B}}B$ so $X\in \mathscr{S}$.
As the union of unions of elements in $\mathscr{B}$ is a union of elements in $\mathscr{B}$, $\mathscr{S}$ is closed under 
abritrary unions. Now, let $B_1,B_2$ be elements of $\mathscr{B}$. If $B_1\cap B_2= \emptyset$ then $B_1\cap B_2 \in \mathscr{S}$. 
Assume that $B_1$ and $B_2$ are not disjoints. Then by definition of a base, for all $x\in B_1\cap B_2$ there exists 
$B_x \in \mathscr{B}$ such that $x\in B_x$ and $B_x \subset B_1\cap B_2$. So
\[ B_1\cap B_2 = \bigcup_{x\in B_1\cap B_2} B_x \ , \] 
and therefore, by definition, $B_1\cap B_2 \in \mathscr{S}$. We conclude that the intersection of two arbitary elements in 
$\mathscr{S}$ is again in $\mathscr{S}$ by using the distributivity of the union with respect to the intersection. 
\end{proof}

\begin{definition}
  We shall say that a base $\mathscr{B}$ on a set $X$ is a \emph{base for a topology $\topology$} on $X$ when the smallest topology 
  containing $\mathscr{B}$ coincides with $\topology$, in other words when $\topology = \topology_\mathscr{B}$.
\end{definition}

The typical usage of the preceding theorem comes from the following result.

\begin{corollary}
Let $\mathscr{B}$ be a base for a topology $\topology$ on $X$. A subset $U$ of $X$ is in $\topology$ if and only if for 
evry $x \in U$ there exists $B \in \mathscr{B}$ such that $x\in B$ and $B \subset U$.
\end{corollary}

\begin{proof}
We showed that any open set for the topology $\topology$ is a union of elements in $\mathcal{B}$. 
Hence if $x \in U$ for $U \in \topology$ then there exists $B\in\mathcal{B}$ such that $x\in B$ and $B \subset U$.
Conversely, if $U$ is some subset of $X$ such that for all $x \in U$ there exists $B_x\in\mathscr{B}$ such that 
$x\in B_x$ and $B_x \subset U$, then $U = \bigcup_{x \in U} B_x$ and thus $U\in \topology$. 
\end{proof}

The last result in this section is a useful tool for showing continuity of a map. 

\begin{proposition} Let $(X,\topology_X)$ and $(Y,\topology_Y)$ be two topological spaces,
  $\mathscr{A}$ a base for the topology $\topology_X$ and  $\mathscr{B}$  a base for the topology $\topology_Y$.
  Assume further that $f:X\to Y$ is a map. Then the following are equivalent:
  \begin{romanlist}
  \item\label{ite:continuity-function} The map $f$ is continuous. 
  \item\label{ite:preimages-open-sets-exhausted-base-elements}
    For every open $V \subset Y$ and all $x\in f^{-1}(V)$ there exists $A \in \mathscr{A}$ such that 
    $x \in U$ and $f(A) \subset V$. 
  \item\label{ite:preimages-base-elements-open}
    For every $B\in \mathscr{B}$ the preimage $f^{-1} (B)$ is open in $X$. 
  \end{romanlist}
\end{proposition}

\begin{proof}
Obviously, \ref{ite:continuity-function} implies \ref{ite:preimages-base-elements-open}. 

Assume that \ref{ite:preimages-base-elements-open} holds and that $V \subset Y$ is open. Let $x\in f^{-1}(V)$ and put
$y=f(x)$. Then $y \in V$. Since  $\mathscr{B}$ is a base for the topology $\topology_Y$ there exists
$B\in \mathscr{B}$ such that $x\in B \subset V$. By assumption $f^{-1}(B)$ is open in $X$ and $x \in f^{-1}(B)$.
Since  $\mathscr{A}$ is a base for the topology $\topology_X$, there exists $A \in \mathscr{A}$
such that $x \in A \subset f^{-1}(B)$. Since $f^{-1}(B) \subset f^{-1}(V)$, \ref{ite:preimages-open-sets-exhausted-base-elements}
follows.

Now assume that  \ref{ite:preimages-open-sets-exhausted-base-elements} holds true. Let $V \subset Y$ be open, and choose
for every $x\in f^{-1}(V)$ a base element $A_x\in \mathscr{A}$ such that $x \in A_x \subset  f^{-1}(V)$. Then
$ f^{-1}(V) = \bigcup_{x\in f^{-1}(V)} A_x $ which is open in $X$. Hence $f$ is continuous. 
\end{proof}
