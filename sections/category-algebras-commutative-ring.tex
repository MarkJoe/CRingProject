% Copyright 2016-2018 Frederic Latremoliere, licensed under GNU FDL v1.3 
% main author: 
%   Markus J. Pflaum 
%
\section{The category of algebras over a commutative ring}
\label{sec:category-algebras-commutative-ring}

\para 
In \Cref{sec:category-rings}  we already got acquainted with unital algebras 
over a commutative ring. Here we want to generalize this concept to 
possibly non-unital algebras. Throughout this section, $R$ will denote a 
commutative ring. 

\subsection*{Definitions}
\begin{definition}
  An $R$-module $A$ together with a map $\mu : A \times A \to A$ 
  called \emph{multiplication} is an $R$-\emph{algebra} if the following 
  properties hold.
  \begin{axiomlist}[Alg]
  \item\label{axiom:bilinearity-multiplication-algebra}
    The multiplication map is $R$-\emph{bilinear} that is the equalities
    \begin{align}
    \nonumber
      \begin{split}
        \mu (a_1 + a_2 , b) & = \mu (a_1,b) + \mu (a_2, b) \ , \\
        \mu (r a , b)  & = r \mu (a,b) \ , \\  
        \mu (a, b_1 + b_2 ) & = \mu (a,b_1) + \mu (a, b_2) \ , \: \text{ and } \\
        \mu ( a , r b) & = r \mu (a,b)   
      \end{split}
    \end{align}  
    hold for all $a,a_1,a_2,b,b_1,b_2 \in A$ and $r\in R$.
  \item\label{axiom:associativity-multiplication-algebra}
    The multiplication map is associative that is 
    \[
      \mu (a,\mu (b,c)) = \mu(\mu (a,b),c) 
    \]  
    for all $a,b,c \in A$.
  \end{axiomlist}
  If in addition Axiom \ref{axiom:multiplicative-identity-algebra}
  below is satisfied, the algebra $A$ is called \emph{unital}, 
  if  Axiom \ref{axiom:commutative-multiplication-algebra} holds true,
  then  $A$ is called \emph{commutative}.
  \begin{axiomlist}[Alg]
  \setcounter{enumi}{2}
    \item\label{axiom:multiplicative-identity-algebra}
    There exists a multiplicative identity in $A$ that is an element  
    $1_A\in A$ such that
    \[
         \mu (1_A,a) = \mu (a,1_A) = a 
    \]
    for all $a\in A$. 
  \item\label{axiom:commutative-multiplication-algebra} 
   Multiplication is commutative that is 
   \[
        \mu (a,b) = \mu (b,a) 
   \]
  for all $a,b\in A$.
  \end{axiomlist}
\end{definition}

\begin{remarks}
  \begin{environmentlist}
  \item
    Given an $R$-algebra $A$ we usually denote the \emph{product} $\mu(a,b)$ of two elements $a,b\in A$ 
    by $a \cdot_A b$, $a \cdot b$ or just by $ab$. 
  \item 
    Since an $R$-algebra $A$ is also a pseudo-ring, a multiplicative identity is uniquely determined 
    by \Cref{thm:basic-properties-pseudo-ring} \ref{ite:uniqueness-multiplicative-identity}. 
    If it exists and when no confusion can arise we usually denote the multiplicative identity in $A$
    just by the symbol $1$.  
  \item 
    As pointed out at the beginning of this chapter, we already have a notion of a unital $R$-algebra. 
    That the one from \Cref{def:unital-algebra} is equivalent to the definition of a unital $R$-algebra
    above is shown by the following result. 
  \end{environmentlist}
 \end{remarks}

 \begin{proposition}
   Assume that $A$ is a ring with multiplication $\mu : A \times A \to A$, $(a,b) \mapsto \mu(a,b)=a\cdot b$ 
   and multiplicative identity $1_A$. Then the following holds true.
   \begin{romanlist}
   \item\label{ite:ring-homomorphism-center-module-structure}
     If  $\varphi : R \to A$ a ring homomorphism with image in the center of $A$,
     then the action  $R \times A \to A$, $(r,a) \mapsto \varphi(r) \cdot a$ gives $A$ the structure 
     of an $R$-module such that  axiom \ref{axiom:bilinearity-multiplication-algebra} is satisfied.
   \item\label{ite:unital-algebra-ring-homomorphism}  
     If $A$ carries an $R$-module structure  $R \times A \to A$, $(r,a) \mapsto ra$ which satisfies 
     axiom \ref{axiom:bilinearity-multiplication-algebra}, then the map 
     $\varphi : R \to A$, $r \mapsto r \cdot 1_A$ is a ring homomorphism with image in the center of $A$.
   \end{romanlist}
 \end{proposition}

 \begin{proof}
   Assume that $\varphi : R \to A$ is a ring homomorphism with image in $\Zentrum{A}$. 
   Since $\varphi$ is a ring homomorphism and since the left and right distributivity laws hold in 
   $A$ one has for all $r,r_1,r_2,s\in R$ and $a,a_1,a_2\in A$
   \begin{align}\nonumber
     \begin{split}
       (rs) a & = \varphi(rs) \cdot a = \left(\varphi(r)\cdot \varphi(s)\right) \cdot a 
       = \varphi(r)\cdot \left( \varphi(s)  \cdot a \right) = 
       \varphi(r)\cdot \left( s a \right) = r (sa) \ , \\
       1_R a & = \varphi(1_R) \cdot a = 1_A \cdot a = a \ , \\
       (r_1 +r_2) a & =  \varphi(r_1+r_2) \cdot a =  \left( \varphi(r_1) + \varphi(r_2) \right) \cdot a 
       =  \varphi(r_1) \cdot a + \varphi(r_2) \cdot a = ra_1 + r_2 a \ , \\
      r (a_1 + a_2) & =  \varphi(r) \cdot (a_1 + a_2) =  \varphi(r) \cdot a_1 + \varphi (r) \cdot a_2
      = r a_1 + r a_2 \ . 
     \end{split}
   \end{align}
   Hence $A$ becomes an $R$-module as claimed. That multiplication on $A$ is $R$-bilinear follows
   from the equalities
   \begin{align}\nonumber
   \begin{split}
      (r a ) \cdot b & =  (\varphi (r) \cdot a) \cdot b  =   \varphi (r) \cdot ( a \cdot b)  =  
      r ( a \cdot b) \ , \: \text{ and} \\
      a \cdot (r b) & =  a \cdot ( \varphi (r) \cdot b)   =   (a \cdot \varphi (r)) \cdot  b  = 
     (\varphi (r) \cdot a ) \cdot  b  = \varphi (r) \cdot (a \cdot  b) = r  ( a\cdot b)  \ .
   \end{split} 
   \end{align}
   Note that hereby we used that $\varphi$ has image in the center of $A$. 
   So \ref{ite:ring-homomorphism-center-module-structure} is proved. 

   To verify \ref{ite:unital-algebra-ring-homomorphism} assume $A$ to be an $R$-module
   with $R$-bilinear multiplication. Define $\varphi : R \to A$ by $\varphi (r) = r 1_A$.
   Then
     \begin{align}\nonumber
   \begin{split}
     \varphi (r+s) & = (r+s) 1_A = r1_A + s1_A = \varphi (r) +\varphi(s) \ , \\
     \varphi (rs) & = (rs) 1_A = r(s1_A) = r \varphi(s) = r (1_A \cdot \varphi (s)) 
     =  (r 1_A) \cdot \varphi (s) =  \varphi (r)\cdot \varphi (s) \ , \\
    \varphi (1_R) & = 1_R 1_A = 1_A \ ,
   \end{split} 
   \end{align}
   so $\varphi$ is a ring homomorphism. 
   By $R$-bilinearity of  $\mu$, the image of $\varphi$ lies in the center of $A$.
   Namely, for all $r \in R$ and $a\in A$
   \[
    \varphi (r) \cdot a = r1_A \cdot a = r (1_A \cdot a) = r (a\cdot 1_A)  = a\cdot (r 1_A) = 
    a \cdot \varphi (r) 
   \] 
 \end{proof}
