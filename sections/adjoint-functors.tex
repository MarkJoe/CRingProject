% Copyright 2010 - 2016 Akhil Matthew, licensed under GNU FDL v1.3 
% main author: 
%   Akhil Mathew 
% contributions by: 
%   Markus J. Pflaum 
%   
\section{Adjoint functors}

\para 
According to MacLane, ``Adjoint functors arise everywhere.'' We shall see
several examples of adjoint functors in this book such as $\Hom$ and the
tensor product. The fact that a functor has an adjoint often immediately
implies useful properties about it like that it commutes with either
limits or colimits. This will lead, for instance, to a conceptual argument
proving right-exactness of the tensor product later on.

For the whole section we suppose that $\category{C}$, $\category{D}$ are categories and 
$F: \category{C} \to \category{D}$, $G: \category{D} \to \category{C}$ 
two (covariant) functors. 

\subsection*{Definition}

\begin{definition} 
The functors $F$, $G$ are \emph{adjoint} if there is a natural isomorphism
\[ \alpha_{c,d} : \Mor_{\category{D}}(Fc, d) \isoarrow  \Mor_{\category{C}}(c, Gd)  \]
whenever $c$ is an object of $\category{C}$ and $d$ an object of 
$\category{D}$. The functor $F$ is said to be the \emph{right adjoint} 
and $G$ is the \emph{left adjoint.}

Here, naturality means that for every morphism $f:c_1\to c_2$ in $\category{C}$
and every morphism $g:d_1\to d_2$ in $\category{D}$ the square 
\begin{equation}
\label{dia:naturality-adjoint-functors}
  \begin{tikzcd}
     \Mor_{\category{D}}(Fc_2, d_1)\ar[dd,"{\Mor_{\category{D}}(Ff,g)=(Ff)^*g_*}",swap] 
     \ar[rr,"{\alpha_{c_2,d_1}}"] 
     & &\Mor_{\category{C}}(c_2,Gd_1) \ar[dd,"{\Mor_{\category{C}}(f,Gg)}=f^*(Gg)_*"] \\ \\
     \Mor_{\category{D}}(Fc_1, d_2) \ar[rr,"{\alpha_{c_1,d_2}}",swap] 
     & & \Mor_{\category{C}}(c_1, Gd_2) 
  \end{tikzcd}
\end{equation}
commutes.
In other words this means that $\alpha$ is a natural isomorphism between
the two functors 
$\Mor_{\category{D}}( F - ,- ), \Mor_{\category{C}} (-, G- ) :
\category{C}^\textup{op} \times \category{D} \to \category{Ens}$ .
\end{definition} 

\begin{examples}
\begin{environmentlist}
\item 
There is a simple pair of adjoint functors between $\category{Ens}$ and $\category{Ab}$. Here,
the first functor sends  a set $S$ to the free abelian group $\Z[S] = \Z^{(S)}$
(see \Cref{def:free-modules} for a discussion of free modules over arbitrary rings), while 
the second, $U$, is the ``forgetful'' functor that sends an abelian group to its underlying set. 
Then $\Z[-]$ and $U$ are adjoints. That is, to give a group-homomorphism
$ \Z^{(S)} \to A$ for some abelian group $A$ is the same as giving a map of sets
$S \to A$. This is precisely the defining property of the free abelian group.
\item 
In fact, most ``free'' constructions are just left adjoints.
For instance, recall the universal property of the free group $F(S)$ on a set $S$ (see
\cite[I.~\S 12]{LanA}): to give a group-homomorphism $F(S) \to G$ for $G$ any group is
the same as choosing an image in $G$ of each $s \in S$.
That is,
\[ \Mor_{\category{Grp}}(F(S), G) = \Mor_{\category{Ens}}(S, U (G)).  \]
This states that the free functor $S \mapsto F(S)$ is left adjoint to the
forgetful functor $U$ from $\category{Grp}$ to $\category{Ens}$.
\item 
The abelianization functor $G \mapsto G^\textup{ab} = G/[G, G]$ from $\category{Grp}
\to \category{Ab}$ is left adjoint to the
inclusion $\category{Ab} \to \category{Grp}$.
That is, if $G$ is a group and $A$ an abelian group, there is  a natural
correspondence between homomorphisms $G \to A$ and $G^\textup{ab} \to A$.
Note that $\category{Ab}$ is a subcategory of $\category{Grp}$ such that the
inclusion admits a left adjoint; in this situation, the subcategory is called
\emph{reflective}.
\end{environmentlist}
\end{examples} 



\subsection*{Adjunctions}

\para 
The fact that two functors are adjoint is encoded by a simple set of algebraic
data between them. 
To see this, suppose $F: \category{C} \to \category{D}$, $G: \category{D} \to \category{C}$ are adjoint functors. For any object $c$ of the category $\category{C}$ we 
know that
\[ \Mor_{\category{D}}(Fc, Fc) \isoarrow \Mor_{\category{C}}(c, GF c),  \]
so that the identity morphism $Fc \to Fc$, which is natural in $c$, 
corresponds to a map $c \to GFc$ also natural in $c$. In other words we obtain a natural transformation
\[ \eta: \id_{\category{C}} \to GF\]
by mapping the object $c$ to the morphism $\eta_c = \alpha_{c,Fc} (\id_{Fc})$. 
This assignment is natural indeed since for a morphism $f :c_1 \to c_2$ in $\category{C}$ the equality 
\begin{align}\nonumber
  \begin{split}
     GFf \, \circ \, & \big( \alpha_{c_1,Fc_1} (\id_{Fc_1})\big)  = \big( (GFf)_* \circ  \alpha_{c_1,Fc_1} \big)(\id_{Fc_1}) 
     =  \big( \alpha_{c_1,Fc_2} \circ (Ff)_* \big) (\id_{Fc_1}) =  \\
     & = \big( \alpha_{c_1,Fc_2} \circ (Ff)^* \big) (\id_{Fc_2}) = 
        \big( f^* \circ \alpha_{c_2,Fc_2} \big) (\id_{Fc_2}) 
       %
       =  \big( \alpha_{c_2,Fc_2} (\id_{Fc_2})\big)  \circ f   
  \end{split}
\end{align}
holds true by commutativity of Diagram \ref{dia:naturality-adjoint-functors}, hence the square
\[
  \begin{tikzcd}
     c_1 \ar[dd,"f",swap] \ar[rr,"{\eta_{c_1}}"] 
     & & GF c_1 \ar[dd,"GFf"] \\ \\
     c_2 \ar[rr,"{\eta_{c_2}}",swap] 
     & & GFc_2  
  \end{tikzcd}
\]
commutes. 

Similarly, we get a natural transformation
\[ \varepsilon:  FG \to \id_{\category{D}}    \]
by mapping an object $d$ of the category $\category{D}$ to the morphism $\alpha_{Gd,d}^{-1} (\id_{Gd})$. 
So the morphism $ \varepsilon_d : FGd \to d$ corresponds to the identity $Gd \to Gd$ under 
the adjoint correspondence. Given a morphism $g:d_1\to d_2$ in $\category{D}$ the square
\[
  \begin{tikzcd}
     FGd_1 \ar[dd,"FGg",swap] \ar[rr,"{\varepsilon_{d_1}}"] 
     & & d_1 \ar[dd,"g"] \\ \\
     FGd_2 \ar[rr,"{\varepsilon_{d_2}}",swap] 
     & & d_2  
  \end{tikzcd}
\]
commutes since by commutativity of Diagram \ref{dia:naturality-adjoint-functors}
\begin{align}\nonumber
  \begin{split}
    g \, \circ \, & \big( \alpha_{Gd_1,d_1}^{-1} (\id_{Gd_1}) \big) =  
    \big( g_* \circ \alpha_{Gd_1,d_1}^{-1}  \big) (\id_{Gd_1}) =
    \big( \alpha_{Gd_1,d_2}^{-1} \circ (Gg)_*\big)  (\id_{Gd_1}) = \\
    & =  \big( \alpha_{Gd_1,d_2}^{-1} \circ (Gg)^* \big)  (\id_{Gd_2})  
    = \big( (FGg)^* \circ \alpha_{Gd_2,d_2}^{-1} \big)  (\id_{Gd_2}) 
    = \big( \alpha_{Gd_2,d_2}^{-1}(\id_{Gd_2}) \big) \circ FGg \ .
  \end{split}
\end{align}
This proves naturality of $\varepsilon$. 

One calls $\eta$ the \emph{unit} and $\varepsilon$ the \emph{counit} of the pair of adjoint 
functors $F$, $G$. The unit and counit are not simply arbitrary.
We are, in fact, going to show that they determine the isomorphisms
$\alpha_{c,d}: \Mor_{\category{D}}(Fc, d) \isoarrow \Mor_{\category{C}}(c, Gd)$. 
This will be a little bit of diagram-chasing.

We know that the isomorphism $\Mor_{\category{D}}(Fc, d) \isoarrow
\Mor_{\category{C}}(c, Gd)$ is natural. In fact, this is the key point.
Let $\phi: Fc \to d$ be any map.
Then there is a morphism $(c, Fc) \to (c, d) $ in the product category
$\category{C}^\textup{op} \times \category{D}$; by naturality of the adjoint
isomorphism, we get a commutative square of sets
\[ \xymatrix{
\Mor_{\category{D}}(Fc, Fc) \ar[r]^{\mathrm{adj}}  \ar[d]^{\phi_*} & \Mor_{\category{C}}(c, GF c)
\ar[d]^{G(\phi)_*} \\
\Mor_{\category{D}}(Fc, d) \ar[r]^{\mathrm{adj}} &  \Mor_{\category{C}}(c, Gd) 
}\]
Here the mark $\mathrm{adj}$ indicates that the adjoint isomorphism is used. 
If we start with the identity $\id_{Fc}$ and go down and right, we get the map 
\( c \to Gd  \)
that corresponds under the adjoint correspondence to $Fc \to d$. However, if we
go right and down, we get the natural unit map $\eta(c): c \to GF c$ followed by $G(\phi)$.

Thus, we have a \emph{recipe} for constructing a map $c \to Gd$ given $\phi: Fc \to
d$:
\begin{proposition}[The unit and counit determines everything]
Let $(F, G)$ be a pair of adjoint functors with unit and counit transformations
$\eta, \varepsilon$.

Then given $\phi: Fc \to d$, the adjoint map $\psi:c \to Gd$ can be constructed simply as
follows.
Namely, we start with the unit $\eta(c): c \to GF c$ and take
\begin{equation} \label{adj1} \psi =  G(\phi) \circ \eta(c): c \to Gd
\end{equation} (here $G(\phi): GFc \to Fd$).
\end{proposition}

In the same way, if we are given $\psi: c \to Gd$ and want to construct a map
$\phi: Fc \to d$, we construct
\begin{equation} \label{adj2} \varepsilon(d) \circ  F(\psi): Fc \to FGd \to   d.
\end{equation}
In particular, we have seen that the \emph{unit and counit morphisms determine
the adjoint isomorphisms.}


Since the adjoint isomorphisms $\Mor_{\category{D}}(Fc, d) \to
\Mor_{\category{C}}(c, Gd)$ and 
$\Mor_{\category{C}}(c, Gd) \to \Mor_{\category{D}}(Fc, d) 
$
are (by definition) inverse to each other, we can determine
conditions on the units and counits.

For
instance, the natural transformation $F \circ \eta$ gives a natural
transformation $F \circ \eta: F \to FGF$, while the natural transformation
$\varepsilon \circ F$ gives a natural transformation $FGF \to F$.
(These are slightly different forms of composition!)

\begin{lemma}  The composite natural transformation $F \to F$ given by
$(\varepsilon \circ F) \circ (F \circ \eta)$ is the identity. 
Similarly, the composite natural transformation
$G \to GFG \to G$ given by $(G \circ \varepsilon) \circ (\eta \circ G)$ is the
identity.
\end{lemma} 


\begin{proof} We prove the first assertion; the second is similar.
Given $\phi: Fc \to d$, we know that we must get back to $\phi$ applying the
two constructions above. The first step (going to a map $\psi: c \to Gd$) is by 
\eqref{adj1}
\( \psi = G(\phi) \circ \eta(c);  \) the second step sends $\psi$ to
$\varepsilon(d) \circ F(\psi)$, by \eqref{adj2}.
It follows that
\[ \phi = \varepsilon(d) \circ F( G(\phi) \circ \eta(c)) = \varepsilon(d) \circ
F(G(\phi)) \circ F(\eta(c)). \]
Now suppose we take $d = Fc$ and $\phi: Fc \to Fc $ to be the identity.
We find that $F(G(\phi))$ is the identity $FGFc \to FGFc$, and consequently we
find
\[ \id_{F(c)} = \varepsilon(Fc) \circ F(\eta(c)). \]
This proves the claim.
\end{proof} 



\begin{definition} 
Let $F: \category{C} \to \category{D}, G: \category{D} \to \category{C}$ be
covariant functors. An \emph{adjunction} is the data of two natural
transformations
\[ \eta: 1 \to GF, \quad \varepsilon: FG \to 1,  \]
called the \emph{unit} and \emph{counit}, respectively, such that the
composites $(\varepsilon \circ F) \circ (F \circ \varepsilon): F \to F$
and $(G \circ \varepsilon) \circ (\eta \circ G)$ are the identity (that is, the
identity natural transformations of $F, G$).
\end{definition} 

We have seen that a pair of adjoint functors gives rise to an adjunction. 
Conversely, an adjunction between $F, G$ ensures that $F, G$ are adjoint, as
one may check: one uses the same formulas \eqref{adj1} and \eqref{adj2} to
define the natural isomorphism.


For any set $S$, let $F(S)$ be the free group on $S$.
So, for instance, the fact that there is a natural map of sets
$S \to F(S)$, for any set $S$, and a natural map of
groups $F(G) \to G$ for any group $G$, determines the adjunction between the
free group functor from $\category{Ens}$ to $\category{Grp}$, and the forgetful
functor $\category{Grp} \to \category{Ens}$.



As another example, we give a criterion for a functor in an adjunction to be
fully faithful.

\begin{proposition} \label{adjfullfaithful}
Let $F, G$ be a pair of adjoint functors between categories $\category{C}, \category{D}$.
Then $G$ is fully faithful if and only if the unit maps $\eta: 1 \to GF$ are
isomorphisms.
\end{proposition} 
\begin{proof} 
We use the recipe \eqref{adj1}.
Namely, we have a map $\Mor_{\category{D}}(Fc, d) \to 
\Mor_{\category{C}}(c, Gd)$ given by 
$\phi \mapsto G(\phi) \circ \eta(c)$. This is an isomorphism, since we have an
adjunction.
As a result, composition with $\eta$ is an isomorphism of hom-sets if and only if $\phi
\mapsto G(\phi)$ is an isomorphism. From this the result is easy to deduce.
\end{proof} 

\begin{example}
For instance, recall that the inclusion functor from $\category{Ab}$ to
$\category{Grp}$ is fully faithful (clear). 
This is a right adjoint to the abelianization functor $G \mapsto G^{ab}$.
As a result, we would expect the unit map of the adjunction to be an
isomorphism, by \Cref{adjfullfaithful}.

The unit map sends an abelian group to its abelianization: this is obviously an
isomorphism, as abelianizing an abelian group does nothing.
\end{example}

\subsection*{Adjoints and (co)limits}
One very pleasant property of functors that are left (resp. right) adjoints is
that they preserve all colimits (resp. limits).

\begin{proposition} \label{adjlimits}
A left adjoint $F: \category{C} \to \category{D}$ preserves colimits. A right
adjoint $G: \category{D} \to \category{C}$ preserves limits.
\end{proposition} 

As an example, the free functor from $\category{Ens}$ to $\category{Ab}$ is a
left adjoint, so it preserves colimits. For instance, it preserves coproducts.
This corresponds to the fact that if $A_1, A_2$ are sets, then $\mathbb{Z}[A_1
\sqcup A_2]$ is naturally isomorphic to $\mathbb{Z}[A_1] \oplus
\mathbb{Z}[A_2]$.

\begin{proof} 
Indeed, this is mostly formal. 
Let $F: \category{C}\to \category{D}$ be a left adjoint functor, with right
adjoint $G$.
Let $f: I \to \category{C}$ be a ``diagram'' where $I$ is a small category. 
Suppose $\colim_I f$ exists as an object of $\category{C}$. The result states
that $\colim_I F \circ f$ exists as an object of $\category{D}$ and can be
computed as 
$F(\colim_I f)$.
To see this, we need to show that mapping out of $F(\colim_I f)$ is what we
want---that is, mapping out of $F(\colim_I f)$ into some $d \in \category{D}$---amounts to
giving compatible $F(f(i)) \to d$ for each $i \in I$.
In other words, we need to show that $\Mor_{\category{D}}( F(\colim_I f), d) =
\lim_I \Mor_{\category{D}}(
F(f(i)), d)$; this is precisely the defining property of the colimit.

But we have
\[ \Mor_{\category{D}}( F(\colim_I f  ), d) = \Mor_{\category{C}}(\colim_I f, Gd)
= \lim_I \Mor_{\category{C}}(fi, Gd) = \lim_I \Mor_{\category{D}}(F(fi), d),
\]
by using adjointness twice. 
This verifies the claim we wanted.
\end{proof} 

The idea is that one can easily map \emph{out} of the value of a left adjoint
functor, just as one can map out of a colimit.



