% Copyright 2010 - 2018 Akhil Mathew, licensed under GNU FDL v1.3 
% main author: 
%   Akhil Mathew 
% modifications and contributions by: 
%   Markus J. Pflaum 
%

\section{The category of rings}\label{sec:category-rings}
%
\subsection*{Definitions and first examples}
Even though we shall mostly work with commutative rings in this book, 
we will introduce the general notion of rings which are allowed to be non-commutative. 
\begin{definition} 
A \emph{ring} is a set $R$ together with an addition map
$+ : R \times R \to R$, a multiplication map $\cdot : R \times R \to R$, and elements $0,1 \in R$
that satisfy the following conditions:

\begin{axiomlist}[Ring]
\item\label{axiom:ring-addition-abelian-group} 
  $R$ together with addition as binary operation and $0$ as neutral element is an abelian group.
\item\label{axiom:ring-multiplication-monoid}  
  $R$ together with multiplication as binary operation and $1$ as neutral element is a monoid
\item\label{axiom:ring-distributivity}   
  Multiplication \emph{distributes} from the left and the right over addition that is
  \begin{axiomlist}[Dist]
    \item[]%[\textup{\sffamily (DistL\hspace{1pt})}]
      \hspace{3em} $r \cdot (s+t) = r \cdot s + r\cdot t \quad $ for all $r,s,t \in R$,  and 
    \item[]%[\textup{\sffamily (DistR)}] 
       \hspace{3em} $(r+s) \cdot t = r \cdot t + s \cdot t \quad $ for all $r,s,t \in R$.
    \end{axiomlist} 
\end{axiomlist}
If, in addition, the following axiom holds, the ring $R$ is called \emph{commutative}:
\begin{axiomlist}[Ring]
\setcounter{enumi}{3}
  \item\label{axiom:ring-commutativity} 
  Multiplication is \emph{commutative} that is 
  $r \cdot s = s \cdot r$ for all  $r,s\in R$.
\end{axiomlist}
We shall typically write $rs$ for $r \cdot s$. Sometimes, when we want to particularly denote 
the structure maps and structure elements of a ring we write a ring as a quintuple $(R,+,\cdot,0,1)$.  

If $R$ is a ring, an \emph{invertible element} or a \emph{unit} is an element $r \in R$, such that 
there exists an element $s \in R$, called (\emph{multiplicative}) \emph{inverse of} $r$, which satisfies
\[
  r \cdot s = 1 \quad\text{and}\quad s \cdot r = 1  \ .
\]
The set of units of a ring $R$ will be denoted by $R^\times$.

Given a ring $R$, a \emph{subring} is a subset $S \subset R$ that contains the zero and identity
elements, is closed under addition and multiplication and is closed under forming additive inverses. 
In other words, $S \subset R$ is a \emph{subring}, if $0,1 \in S$
and if for all $r,s\in S$ the elements $r+s$, $-r$ and $rs$ are in $S$ as well. 

Following \cite[p.~98]{BouAI},  a \emph{pseudo-ring} (or in other words \emph{non-unital-ring}) is a set $R$ 
together with binary operations $+$ and $\cdot$ and an element $0$ such that
the Axioms \ref{axiom:ring-addition-abelian-group} and \ref{axiom:ring-distributivity} are satisfied 
and such that $\cdot$ is associative that is \ref{axiom:group-associativity} holds true. 
A pseudo-ring $R$ is called \emph{commutative} if 
Axiom \ref{axiom:ring-commutativity} is satisfied as well. 
A subset $S$ of a pseudo-ring $R$ is called a \emph{sub-pseudo-ring}, if it contains the zero element,
is closed under addition and multiplication, and is closed under forming additive inverses. 
Sometimes we write a pseudo-ring as a quadrupel $(R,+,\cdot,0)$ to include the structure maps and its zero element. 

If $R$ is a pseudo-ring, the \emph{center}  of $R$ is defined as the set of all $r\in R$ commuting with
all ring elements that is as the set 
\[
  \Zentrum{R} := \big\{ r \in R \mid rs = sr \text{ for all } s\in R \big\} \ .
\]
\end{definition} 

The following result is essentially a repetition of \Cref{thm:basic-algebraic-properties-ring}.

\begin{proposition}\label{thm:basic-properties-pseudo-ring}
Let $R$ be a pseudo-ring. Then
\begin{romanlist}
\item\label{ite:zero-is-zero-divisor}
  $0 \cdot r = r \cdot 0 = 0$ for all $r\in R$.
\item 
  $(-r)s = r(-s)= - (rs)$  for all $r,s\in R$.
\item 
  If $R$ possesses a multiplicative identity $1$, then $(-1)^2 = 1$. 
\item\label{ite:uniqueness-multiplicative-identity}
  A multiplicative identity  in $R$ is uniquely determined. 
\item
  Assume that $R$ possesses an identity element. Then the inverse for an invertible $r\in R$ is uniquely determined.
  If it exists, the inverse of $r$ is denoted by $r^{-1}$.
\end{romanlist}
\end{proposition}
\begin{proof}
\begin{adromanlist}
\item First compute using associativity, distributivity and that $0$ is a zero element:
  \[ 0 \cdot r = (0+0) \cdot r =  (0\cdot r) + (0 \cdot r)  \ . \]
  Adding $-(0\cdot r)$ on both sides gives $0= 0\cdot r$. By an analogous argument we obtain  $0= r \cdot 0$.
\item By \ref{ite:zero-is-zero-divisor} we obtain
  \[ 0 = 0 \cdot s = (r + (-r)) \cdot s = r s + (-r)s \ , \]
  which entails $(-r)s = - (rs)$. Similarly, one shows  $r(-s) = - (rs)$.
\item
  If $1$ is a multiplicative identity, then $0 = 0 \cdot (-1) = (1 + (-1)) \cdot (-1) = (-1) + (-1)^2$ 
  which entails the claim by adding $1$ on both sides. 
\item Assume that $1$ and $1'$ are two identity elements in $R$. Then 
  \[ 1 = 1\cdot 1' = 1' \ .\]
\item Let $R$ be a ring with identity $1$ and $s,s' \in R$ be two inverses of  $r$. Then 
  \[ s = s \cdot 1 = s \cdot ( r \cdot s') = (s \cdot r ) \cdot s' = 1 \cdot s' = s' \ . \]
\end{adromanlist}
\end{proof}

\begin{examples} 
\begin{environmentlist}
\item 
  The \emph{zero ring} or \emph{trivial ring} is the ring with underlying set $\{ 0 \}$. 
  Its identity element coincides with $0$, and it is obviously commutative. 
\item
  The simplest non-trivial example of a  ring is the commutative ring
  $\Z_2 = \{ 0,1\}$.
  Note that as a consequence of the ring axioms and the fact that $0 \neq 1$ one has $-1 = 1$ in $\Z_2 $. 
\item
  The most important example of a commutative ring is the ring of integers $\Z$.
\item
  The sets $\Q$, $\R$, and $\C$ of rational, real, and complex numbers, respectively, form all commutative rings.
\item 
  The set $\H$ of quaternions is a ring which is not commutative.
\end{environmentlist}
\end{examples} 

\begin{example}
  The center $\Zentrum{R}$ of a pseudo-ring $R$ is a sub-pseudo-ring of $R$ by 
  \Cref{thm:basic-properties-pseudo-ring}. It is commutative by definition. Moreover, if $R$ 
  possesses an identity element, then $\Zentrum{R}$ is even a subring. 
\end{example}

\begin{examples}\label{ex:function-rings}
The following are examples of function rings.
\begin{environmentlist}
\item\label{item:functions-set-ring}
Let $X$ be a set and $R$ a pseudo-ring. The set $R^X$ of functions $f:X \to R$ is a
pseudo-ring. Hereby, addition and multiplication of functions $f,g:X \to R$ are defined pointwise: 
$(f+g) (x) := f(x) + g(x)$ and $(f\cdot g) (x) := f(x) \cdot g(x) $ for $x\in X$. 
Obviously, $R^X$ with addition as binary operation then becomes an abelian group, where 
the zero function $0_X: X \to R$, $x \mapsto 0$ serves as neutral element, and the additive inverse $-f$ of $f\in R^X$ 
is given by $(-f) (x) := - f(x)$ for  $x\in R$.  Associativity and commutativity of addition in $R^X$
hold true because they hold pointwise over each $x\in X$.   
Likewise, multiplication in $R^X$ is associative. The distributivity law holds in $R^X$ also, because it holds pointwise 
when evaluating at  $x\in X$. So $R^X$  becomes a pseudo-ring.
If $R$ is even a ring, the function  $1_X : X \to R$, $x \mapsto 1$ serves as an identity element, so $R^X$ then
is a ring as well. 
\item\label{item:free-ring-module-set}
A sub-pseudo-ring of $R^X$ (independently of wether $R$ is a ring or pseudo-ring) is given by 
the subset 
\[ 
  R^{(X)}:= \big\{ f\in R^X\bigmid f(x) \neq 0 \text{ for at most finitely many } x \in X \big\} \ .
\]
This follows immediately from the observation that the sum and the product of two elements 
$f,g \in R^{(X)}$ lie again in  $R^{(X)}$, that  $0_X \in R^{(X)}$, and that $R^{(X)}$ contains 
with an element $f$ also its negative $-f$.  
Unless $X$ is finite and $R$ a ring, the pseudo-ring $R^{(X)}$ is not unital or in other 
words not a ring. 
\item\label{item:continuous-real-functions-topological-space}
If $X$ is a topological space and $R =\R$, the subspace 
\[ \shContFcts (X) := \{ f \in \R^X \mid f \text{ is continuous} \}\]
is a subring of $\R^X$, since the constant functions $0_X$ and $1_X$ are continuous, and since the sum and product of two 
real-valued functions on $X$ are again continuous. 
\item\label{item:smooth-real-functions-manifold}
If $M$ is a smooth manifold, the subspace 
\[ \shSmoothFcts
 (M) := \{ f \in \shContFcts (M) \mid f \text{ is smooth}\} \]
is a subring of $\shContFcts (M)$, since  the constant function $1_X$ is smooth, and since the sum and product of two 
real-valued smooth functions on $M$ is again smooth. 
\end{environmentlist}
\end{examples}

\begin{example}\label{ex:polynomial-ring} 
Let $R$ be a commutative ring. One defines $R[x]$, the 
\emph{ring  of polynomials in one variable over} $R$, as follows. 
As a set, $R[x]$ coincides with $R^{(\N)}$. For an element $a\in R[x]$ denote by $a_n$ for
every $n\in \N$ its $n$-th component, that means let $a = (a_n)_{n\in \N}$. 
Using this agreement, the sum and product of two elements $a,b \in R[x]$ are defined by
\begin{equation*}
  \begin{split}
     (a + b)_n & \, := (a_n + b_n) \text{ for all } n\in \N , \text{ and } \\
     (a \cdot b )_n & \, := \sum_{k+l=n} a_k b_l \text{ for all } n\in \N \ . 
  \end{split}
\end{equation*}
By Example \ref{ex:function-rings} \ref{item:free-ring-module-set}, $(R[x],+,0_\N)$ is an abelian group
with zero element $0_\N : \N \to R$, $n \mapsto 0$. Let us show that $\cdot$ is an associative and 
commutative operation on  $R[x]$. To this end let $a,b,c \in R[x]$ and compute for $n\in \N$
using associativity of multiplication in $R$
\begin{align}\nonumber
\begin{split}
   ((a \cdot b ) \cdot c)_n & = \sum_{l+k=n} \sum_{i+j=l} (a_i b_j)c_k 
   = \sum_{i+j+k=n} (a_i b_j)c_k = \\ & =  \sum_{i+j+k=n} a_i (b_j c_k) =
    \sum_{i+l=n} \sum_{j+k=l} a_i (b_jc_k) =  (a \cdot (b  \cdot c))_n \ .
\end{split}
\end{align}
Next verify that by commutativity of $R$
\begin{align}\nonumber
\begin{split}
   (a \cdot b )_n & = \sum_{k+l=n} a_k b_l 
   = \sum_{k+l=n} b_l a_k = (b \cdot a)_n \ .
\end{split}
\end{align}
Denote by $1_\N$ the element of $R[x]$ defined by $(1_\N)_0 =1$ and 
$(1_\N)_n =0$ for $n \in \gzN$. One checks immediately that $a \cdot 1_\N = 1_\N \cdot a =a$ 
for all $a\in R[x]$. Hence $(R[x],+,\cdot,0_\N,1_\N)$ is a commutative ring as claimed.
Let us now denote by $x$ the element of $R[x]$ uniquely defined by the property that
its $n$-th component is $1$ for $n=1$ and $0$ otherwise. Moreover, for $r\in R$ 
and $a \in R[x]$ denote by $ra$ the element with components $(ra)_n = r a_n$, $n\in \N$. 
With this agreement and the observation that the $k$-th component of $x^n$ is $1$ for $k=n$ 
and $0$ otherwise one checks that every $a\in R[x]$ can be written in the form 
\[
 a = \sum_{n\in\N} a_n x^n \ .
\]
Note that the sum on the right hand side is finite indeed, since only finitely many $a_n$ are 
allowed to be nonzero. The representation of $a$ in the form $\sum_{n\in\N} a_n x^n$ is even unique.
This follows from the observation that the $k$-th component of a sum $ \sum_{n\in\N} b_n x^n$,
where only finitely many $b_n\in R$ are  nonzero, is the sum of the $k$-th components 
of the elements $b_nx^n$ and that the $k$-th component of $b_nx^n$ is $b_k$ if $n=k$ and $0$ 
otherwise. We will later see how one can embed $R$ into $R[x]$ and how polynomial rings 
$R[x_1, \ldots, x_n]$ in several variables $x_1,\ldots ,x_n$ are defined. 
\end{example} 
%
%
The class of rings forms a category. Its morphisms are called ring homomorphisms.
Let us formulate this in more detail. 
\begin{definition}
A \emph{morphism of pseudo-rings} is a map $f : R \to S$ between pseudo-rings 
$(R,+\sub{R},\cdot\sub{R},0\sub{R})$ and $(S,+\sub{S},\cdot\sub{S},0\sub{S})$ 
that respects addition and multiplication. That is
\begin{axiomlist}[Ring]
\setcounter{enumi}{4}
  \item\label{axiom:ring-homomorphism-additivity} $f(x +\sub{R} y) = f(y) +\sub{S} f(y)$ for all $x, y \in R$.
  \item\label{axiom:ring-homomorphism-multiplicativity} $f(x \cdot\sub{R}y) = f(x)\cdot\sub{S}f(y)$ for all $x, y \in R$.
\end{axiomlist}

If $R$ and $S$ are rings, a morphism of pseudo-rings $f : R \to S$ which preserves the identity elements 
is called a \emph{ring homomorphism}. 
More precisely, $f : R \to S$ is a \emph{ring homomorphism}
if it satisfies Axioms \ref{axiom:ring-homomorphism-additivity} and 
\ref{axiom:ring-homomorphism-multiplicativity} and in addition the axiom 
\begin{axiomlist}[Ring]
\setcounter{enumi}{7}
  \item\label{axiom:ring-homomorphism-preserving-identity-elements} 
   $f(1\sub{R}) = 1\sub{S}$, where $1\sub{R}$ and $1\sub{S}$ are the respective identity elements.
\end{axiomlist}
\end{definition}

\begin{proposition}
Let $f : R \to S$ be  a morphism of pseudo-rings. Then
\begin{axiomlist}[Ring]
\setcounter{enumi}{6}
\item $f(0_R) = 0_S$, where $0_R$ and $0_S$ are the respective zero elements.
\end{axiomlist}
\end{proposition}

\begin{proof}
  By additivity of $f$ one has $f(0_R) = f (0_R +_R 0_R) = f(0_R) +_S f(0_R)$. From this one concludes 
  $f(0_R) = 0_S$ since $(S,+_S,0_S)$ is an abelian group, so has the cancellation property. 
\end{proof}

\begin{thmanddef}
\begin{environmentlist}
\item   
  The identity map $\id_R$ on a ring $R$ is a ring homomorphism.  
  If $R$ is a pseudo-ring, $\id_R$ is a morphism of pseudo-rings.
\item
  Let $R$, $S$ and $T$ be three rings and 
  $f:R\rightarrow S$ and $S:Y\rightarrow T$ ring homomrphisms. 
  Then $g\circ f$ is a ring homomorphism. If $R$, $S$, $T$ are pseudo-rings 
  and $f$, $g$ morphisms of pseudo-rings, then so is  $g\circ f$. 
\item 
  Rings as objects together with ring homomorphisms as morphisms form a  category.
  It is called the \emph{category of rings} and will be denoted by $\cat{Ring}$.  
  Analogously, one obtains the category $\category{PRing}$ of pseudo-rings
  and morphisms of pseudo-rings.  
  The category $\category{Ring}$ can be understood in a canonical way as
  a subcategory of $\category{PRing}$.
\end{environmentlist}
\end{thmanddef}

\begin{proof}
 The identity map on a (pseudo-) ring obviously preserves all structure maps and neutral elements. 
 If $f$ and $g$ are morphisms of pseudo-rings, then, for all $x,y \in R$ 
 \begin{align}\nonumber
   \begin{split}
     ( g \circ f )(x +_R y ) & = g \big(  f (x +_R y ) \big) =  g \big(  f (x) +_S f(y) \big) 
       = g \big(  f (x) \big) +_T g \big( f(y) \big) = \\ &=  (g \circ f) (x) +_T  (g \circ f) (y ) \ , \\
     g \circ f (x \cdot_R y ) & = g \big(  f (x \cdot_R y ) \big) =  g \big(  f (x) \cdot_S f(y) \big)
     = g \big(  f (x) \big) \cdot_T g \big( f(y) \big) = \\ &=  (g \circ f) (x) \cdot_T ( g \circ f) (y ) \ ,
   \end{split}
 \end{align}
 hence $g\circ f$ is a morphism of pseudo-rings. If $R$, $S$, and $T$ are even rings, and 
 $f$, $g$ ring homomorphisms, one checks
 \[
   (g \circ f) (1_R) = g \big( f(1_R) \big) = g (1_S) = 1_T \ ,
 \]
 so  $g\circ f$  is a ring homomorphism. It is now clear that pseudo-rings together with morphisms of
 pseudo-rings and rings together ring homomorphisms form categories $\category{PRing}$ and
 $\category{Ring}$, respectively. The canonical embedding of $\category{Ring}$ into $\category{PRing}$
 is obtained by mapping a ring $(R,+,\cdot,0,1)$ to the pseudo-ring $(R,+,\cdot,0)$  
 or in other words by forgetting the multiplicative identity. Note that this is an embedding indeed 
 since by \Cref{thm:basic-properties-pseudo-ring} \ref{ite:uniqueness-multiplicative-identity} 
 identity elements in a ring are uniquely determined.  
\end{proof}

\subsection*{Unital algebras over a commutative ring}

\para 
The philosophy of Grothendieck, as expounded in his EGA \cite{EGA}, is that one should
always do things in a relative context. This means that instead of working
with objects, one should work with \emph{morphisms} of objects. Motivated by
this, we introduce:

\begin{definition}\label{def:unital-algebra} 
Given a commutative ring $R$, a \emph{unital $R$-algebra} is a ring $A$ together with a
morphism of rings (the \emph{structure morphism}) $R \to Z(A) \subset A$. 
In other words, the structure morphism $R \to A$ has image in the center of 
the ring $A$. A unital $R$-algebra $A$ is called \emph{commutative} if $A$ is a commutative ring. 

A morphism between $R$-algebras is a ring homomorphism that is required to commute with the structure
morphisms. So if $A$ is an $R$-algebra, then $A$ is not only a ring, but there is a way
to multiply elements of $A$ by elements of $R$. Namely, to multiply $a \in A$
with $x \in R$, take the image of $x$ in $A$, and multiply that by $a$.

For instance, any ring is an algebra over any subring.

\end{definition} 

We can think of an $A$-algebra as an arrow $A \to R$, and a morphism from $A
\to R$ to $A \to S$ as a commutative diagram
\[ \xymatrix{
R \ar[rr] & & S \\
& \ar[lu] A \ar[ru]
}\]
This is a special case of the \emph{undercategory} construction.

If $B$ is an $A$-algebra and $C$ a $B$-algebra, then $C$ is an $A$-algebra in a
natural way. Namely, by assumption we are given morphisms of rings $A \to B$
and $B \to C$, so composing them gives the structure morphism $A \to C$ of $C$
as an $A$-algebra. 


\begin{example} 
Every ring is a $\Z$-algebra in a natural and unique way. There is a
unique map (of rings) $\mathbb{Z} \to R$ for any ring $R$ because a
ring-homomorphism is required to preserve the identity. 
In fact, $\Z$ is the \emph{initial object} in the category of rings:
this is a restatement of the preceding discussion.
\end{example}

\begin{example} 
If $R$ is a ring, the polynomial ring $R[x]$ is an $R$-algebra in a natural
manner. Each element of $R$ is naturally viewed as a ``constant polynomial.''
\end{example} 

\begin{example} 
The field of complex numbers $\C$ is an $\R$-algebra.
\end{example} 

\begin{example}
For any ring $R$, we can consider the polynomial ring $R[x_1, \ldots, x_n]$
which consists of the polynomials in $n$ variables with coefficients in $R$.
This can be defined inductively as $(R[x_1, \dots, x_{n-1}])[x_n]$, where the
procedure of adjoining a single variable comes from the previous
\Cref{ex:polynomial}.
\end{example}

We shall see a more general form of this procedure in \Cref{ex:groupring}.


Here is an example that generalizes the case of the polynomial ring.
\begin{example} 
\label{ex:groupring}
If $R$ is a ring and $G$ a commutative monoid,\footnote{That is, there is a
commutative multiplication on $G$ with an identity element, but not
necessarily with inverses.} then the set
$R[G]$ of formal finite sums $\sum r_i g_i$ with $r_i \in R, g_i \in G$ is a
commutative ring, called the \textbf{monoid ring} or \textbf{group ring} when
$G$ is a group.
Alternatively, we can think of elements of $R[G]$ as infinite sums $\sum_{g \in
G} r_g g$ with $R$-coefficients, such that almost all the $r_g$ are zero.
We can define the multiplication law such that 
\[ \left(\sum r_g g\right)\left( \sum s_g g\right) = 
\sum_h \left( \sum_{g g' = h} r_g s_{g'}  \right) h.
\]
This process is called \emph{convolution.} We can think of the multiplication
law as extended the group multiplication law (because the product of the
ring-elements corresponding to $g, g'$ is the ring element corresponding to
$gg' \in G$).

The case of $G = \N$ is the polynomial ring. 
In some cases, we can extend this notion to formal infinite sums, as in the
case of the formal power series ring; see \cref{powerseriesring} below.
\end{example} 

\begin{remark}
\label{integersinitial}
The ring $\mathbb{Z}$ is an \emph{initial object} in the category of rings.
That is, for any ring $R$, there is a \emph{unique} morphism of rings
$\mathbb{Z} \to R$. We discussed this briefly earlier; show more generally that
$A$ is the initial object in the category of $A$-algebras for any ring $A$.
\end{remark} 

\begin{remark} 
The ring where $0=1$ (the \textbf{zero ring}) is a \emph{final object} in the category of rings. That
is, every ring admits a unique map to the zero ring.	
\end{remark} 

\begin{remark}
\label{corepresentable}
Let $\mathcal{C}$ be a category and $F: \mathcal{C} \to \mathbf{Sets}$  a
covariant functor. Recall that $F$ is said to be \textbf{corepresentable} if
$F$ is naturally isomorphic to $X \to \hom_{\mathcal{C}}(U, X)$ for some
object $U \in \mathcal{C}$. For instance, the functor sending everything to a
one-point set is corepresentable if and only if $\mathcal{C}$ admits an
initial object.

Prove that the functor  $\category{Ring} \to \mathbf{Sets}$ assigning to each ring its underlying set is
representable. (Hint: use a suitable polynomial ring.)
\end{remark} 


The category of rings is both complete and cocomplete. To show this in full
will take more work, but we can here describe what 
certain cases (including all limits) look like.
As we saw in \cref{corepresentable}, the forgetful functor $\category{Ring} \to
\mathbf{Sets}$ is corepresentable. Thus, if we want to look for limits in the
category of rings, here is the approach we should follow: we should take the
limit first of the underlying sets, and then place a ring structure on it in
some natural way.

\begin{example}[Products]
The \textbf{product} of two rings $R_1, R_2$ is the set-theoretic product $R_1
\times R_2$ with the multiplication law $(r_1, r_2)(s_1, s_2) = (r_1 s_1, r_2
s_2)$. It is easy to see that this is a product in the category of rings. More
generally, we can easily define the product of any collection of rings.
\end{example} 

To describe the coproduct is more difficult: this will be given by the
\emph{tensor product} to be developed in the sequel.

\begin{example}[Equalizers]
Let $f, g: R \rightrightarrows S$ be two ring-homomorphisms. Then we can
construct the \textbf{equalizer} of $f,g$ as the subring of $R$ consisting of
elements $x \in R$ such that $f(x) = g(x)$. This is clearly a subring, and one
sees quickly that it is the equalizer in the category of rings.
\end{example} 

As a result, we find:
\begin{proposition} 
The category $\category{Ring}$ is complete.
\end{proposition} 

As we said, we will not yet show that $\category{Ring}$ is cocomplete. But we
can describe filtered colimits.  In fact, filtered colimits will be constructed
just as in the set-theoretic fashion. That is, the forgetful functor
$\category{Ring} \to \mathbf{Sets}$ commutes with \emph{filtered} colimits
(though not with general colimits). 
\begin{example}[Filtered colimits]
Let $I$ be a filtering category, $F: I \to \category{Ring}$ a functor. We can
construct $\varinjlim_I F$ as follows. An object is an element $(x,i)$ for $i
\in I$ and $x \in F(i)$, modulo equivalence; we say that $(x, i)$ and $(y, j)$
are equivalent if there is a $k \in I$ with maps $i \to k, j \to k$ sending
$x,y$ to the same thing in the ring $F(k)$.

To multiply $(x, i)$ and $(y,j)$, we find
some $k \in I$ receiving maps from $i, j$, and replace $x,y$ with elements of
$F(k)$. Then we multiply those two in $F(k)$. One easily sees that this is a
well-defined multiplication law that induces a ring structure, and that what we
have described is in fact the filtered colimit.

\end{example} 






\subsection*{Zerodivisors}


Let $R$ be a commutative ring.
\begin{definition} 
If $r \in R$, then $r$ is called  a \textbf{zero divisor} if there is $s \in R, s
\neq 0$ with $sr = 0$. Otherwise $r$ is called a \textbf{non-zero-divisor.}
\end{definition} 

As an example, we prove a basic result on the zero divisors in a polynomial ring.

\begin{proposition}
Let $A=R[x]$. Let $f=a_nx^n+\cdots +a_0\in A$. If there is a non-zero polynomial $g\in
A$ such that $fg=0$, then there exists $r\in R\smallsetminus\{0\}$ such that $f\cdot
 r=0$.
\end{proposition}
So all the coefficients are zero divisors.
\begin{proof}
 Choose $g$ to be of minimal degree, with leading coefficient $bx^d$. We may assume
 that  $d>0$. Then $f\cdot b\neq 0$, lest we contradict minimality of $g$. We must have
$a_i g\neq 0$ for some $i$. To see this, assume that $a_i\cdot g=0$, then $a_ib=0$ for
all $i$ and then $fb=0$. Now pick $j$ to be the largest integer such that $a_jg\neq
   0$. Then $0=fg=(a_0 + a_1x + \cdots a_jx^j)g$, and looking at the leading coefficient,
   we get $a_jb=0$. So $\deg (a_jg)<d$. But then $f\cdot (a_jg)=0$, contradicting
   minimality of $g$.
 \end{proof}

\begin{remark} 
The product of two non-zero-divisors is a non-zero-divisor, and the product of two
zero divisors is a zero divisor. It is, however, not necessarily true that the
\emph{sum} of two zero divisors is a zero divisor.
\end{remark} 

\section{Further examples}

We now illustrate a few important examples of 
commutative rings. The section is in large measure an advertisement for why
one might care about commutative algebra; nonetheless, the reader is
encouraged at least to skim this section.

\subsection*{Rings of holomorphic functions}

The following subsec may be omitted without impairing understanding.

There is a fruitful analogy in number theory between the rings $\mathbb{Z}$ and
$\mathbb{C}[t]$, the latter being the polynomial ring over $\mathbb{C}$ in one
variable (\cref{polynomial}).  Why are they analogous? Both of these rings have a theory of unique
factorization:  that is, factorization into primes or irreducible polynomials. (In the
latter, the irreducible polynomials have degree one.)
Indeed we know:
\begin{enumerate}
\item Any nonzero integer factors as a product of primes (possibly times $-1$). 
\item Any  nonzero polynomial factors as a product of an element of
$\mathbb{C}^* =\mathbb{C} - \left\{0\right\}$ and polynomials of the form $t -
a, a \in \mathbb{C}$.
\end{enumerate}


There is another way of thinking of $\mathbb{C}[t]$ in terms of complex
analysis.  This is equal to the ring of holomorphic functions on $\mathbb{C}$
which are meromorphic at infinity.  
Alternatively, consider the Riemann sphere $\mathbb{C} \cup \{ \infty\}$; then the ring $\mathbb{C}[t]$
consists of meromorphic functions on the sphere whose poles (if any) are at
$\infty$. This description admits generalizations. 
Let $X$ be a
Riemann surface.  (Example: take the complex numbers modulo a lattice, i.e. an
elliptic curve.)
Suppose that $x \in X$. Define $R_x$ to be the ring of meromorphic functions on $X$
which are allowed poles only at $x$ (so are everywhere else holomorphic). 

\begin{example} Fix the notations of the previous discussion. 
Fix $y \neq x \in X$. Let $R_x$ be the ring of meromorphic functions on the
Riemann surface $X$ which are holomorphic on $X - \left\{x\right\}$, as before.
Then the collection of functions that vanish at $y$ forms an
\emph{ideal} in $R_x$. 

There are lots of other ideals. For instance, fix two
points $y_0, y_1 \neq x$; we look at the ideal of $R_x$ that vanish at both $y_0, y_1$.
\end{example} 


\textbf{For any Riemann surface $X$, the conclusion of Dedekind's theorem
(\cref{ded1}) applies.  } In other
words, the ring  $R_x$ as defined in the example admits  unique factorization of
ideals. We shall call such rings \textbf{Dedekind domains} in the future.

\begin{example} Keep the preceding notation.

Let $f \in R_x$, nonzero. By definition, $f$ may have a pole at $x$, but no poles elsewhere. $f$ vanishes
at finitely many points $y_1, \dots, y_m$. When $X$ was the Riemann sphere,
knowing the zeros of $f$ told us something about $f$. Indeed, in this case
$f$ is just a
polynomial, and we have a nice factorization of $f$ into functions in $R_x$ that vanish only
at one point. In general Riemann surfaces, this
is not generally possible.  This failure turns out to be very interesting.

Let $X = \mathbb{C}/\Lambda$ be an elliptic curve (for $\Lambda \subset
\mathbb{C}^2$ a lattice), and suppose $x = 0$. Suppose we
are given $y_1, y_2, \dots, y_m \in X$ that are nonzero; we ask whether there
exists a function $f \in R_x$ having simple zeros at $y_1, \dots, y_m$ and nowhere else.
The answer is interesting, and turns out to recover the group structure on the
lattice.
\end{example}

\begin{proposition} 
A function $f \in R_x$ with simple zeros only at the $\left\{y_i\right\}$ exists if and only if $y_1 + y_2 + \dots + y_n = 0$ (modulo $\Lambda$).

\end{proposition} 
So this problem of finding a function with specified zeros is equivalent to
checking that the specific zeros add up to zero with the group structure.

In any case, there might not be such a nice function, but we have at least an
ideal $I$ of functions that have zeros (not necessarily simple) at $y_1, \dots,
y_n$.  This ideal has unique factorization into the ideals of functions
vanishing at $y_1$, functions vanishing at $y_2$, so on.  
 

\subsection*{Ideals and varieties}

We saw in the previous subsec that ideals can be thought of as the
vanishing of functions. This, like divisibility, is another interpretation,
which is particularly interesting in algebraic geometry.


Recall the  ring $\mathbb{C}[t]$ of complex polynomials discussed in the
last subsec. More generally, if $R$ is a ring,  we saw in
\cref{polynomial} that the set $R[t]$ of polynomials with coefficients
in $R$
is a ring.  This is a construction that
can be iterated to get a polynomial ring in several variables over $R$.

\begin{example} 
Consider the polynomial ring $\mathbb{C}[x_1, \dots, x_n]$. Recall that before
we thought of the ring $\mathbb{C}[t]$ as a ring of meromorphic functions.  
Similarly each element of the polynomial ring $\mathbb{C}[x_1, \dots, x_n]$
gives a function $\mathbb{C}^n \to \mathbb{C}$; we can think of the polynomial
ring as sitting inside the ring of all functions $\mathbb{C}^n \to \mathbb{C}$.

A question you might ask: What are the ideals in this ring?  One way to get an
ideal is to pick a point $x=(x_1, \dots, x_n) \in \mathbb{C}^n$; consider the
collection of all functions $f \in \mathbb{C}[x_1, \dots, x_n]$ which vanish on
$x$; by the usual argument, this is an ideal.

There are, of course, other ideals. More generally, if $Y \subset
\mathbb{C}^n$, consider the collection of polynomial functions $f:
\mathbb{C}^n \to \mathbb{C}$ such that $f \equiv 0$ on
$Y$.  This is easily seen to be an ideal in the polynomial ring. We thus have a
way of taking a subset of $\mathbb{C}^n$ and producing an ideal.
Let $I_Y$ be the ideal corresponding to $Y$.  

This construction is not injective. One can have $Y \neq Y'$ but $I_Y = I_{Y'}$. For instance, if $Y$ is dense in
$\mathbb{C}^n$, then $I_Y = (0)$, because the only way a continuous function on
$\mathbb{C}^n$ can vanish on $Y$ is for it to be zero.

There is a much closer connection in the other direction. You might ask whether
all ideals can arise in this way. The quick answer is no---not even when $n=1$. The ideal $(x^2) \subset \mathbb{C}[x]$ cannot be obtained
in this way.  It is easy to see that the only way we could get this as $I_Y$ is
for $Y=\left\{0\right\}$, but $I_Y$ in this case is just $(x)$, not $(x^2)$.
What's going wrong in this example is that $(x^2)$ is not a \emph{radical}
ideal.
\end{example} 

\begin{definition}\label{def-radical-ideal} 
An ideal $I \subset R$ is \textbf{radical} if whenever $x^2 \in I$, then $x \in
I$.
\end{definition} 

The ideals $I_Y$ in the polynomial ring are all radical.  This is obvious.
You might now ask whether this is the only obstruction. We now state a theorem
that we will prove later.

\begin{theorem}[Hilbert's Nullstellensatz] If $I \subset \mathbb{C}[x_1, \dots,
x_n]$ is a radical ideal, then $I = I_Y$ for some $Y \subset \mathbb{C}^n$. In
fact, the canonical choice of $Y$ is the set of points where all the functions
in $Y$ vanish.\footnote{Such a subset is called an algebraic variety.}
\end{theorem} 


This will be one of the highlights of the present course. But before we can
get to it, there is much to do.

\begin{remark} 
Assuming the Nullstellensatz, show that any \emph{maximal} ideal in the
polynomial ring $\mathbb{C}[x_1, \dots, x_n]$ is of the form 
$(x_1-a_1, \dots, x_n-a_n)$ for $a_1, \dots, a_n \in \mathbb{C}$. An ideal of a
ring is called \textbf{maximal} if the only ideal that contains it is the
whole ring (and it itself is not the whole ring).

As a corollary, deduce that if $I \subset \mathbb{C}[x_1, \dots, x_n]$ is a
proper ideal (an ideal is called \textbf{proper} if it is not equal to the
entire ring), then there exists $(x_1, \dots, x_n) \in \mathbb{C}^n$ such that
every polynomial in $I$ vanishes on the point $(x_1, \dots, x_n)$. This is
called the \textbf{weak Nullstellensatz.}
\end{remark} 

