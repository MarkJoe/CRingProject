% Copyright 2016-2018 Markus J. PFlaum, licensed under GNNU FDL v1.3 
% main author: 
%   Markus J. Pflaum 
%
\section{Additive categories}\label{sec:additive-categories}

\begin{definition}
  By a \emph{pre-additive category} one understands a category $\category{A}$
  \emph{enriched} over the category of abelian groups. This means that for each 
  pair of objects $A,B$ in $\category{A}$ the morphism set $\Mor (A,B)$ 
  carries an abelian group structure 
  \[
    +_{(A,B)} : \Mor (A,B) \times \Mor (A,B) \rightarrow \Mor (A,B), \quad (f,g) 
    \mapsto f + g
  \]
  such that composition of morphisms in $\category{A}$ is bilinear in the 
  following sense: 
  \begin{enumerate}
  \item[(BL)] 
  \label{axiom:bilinearity-addition-pre-additive-category}   
     If $A,B,C$ are objects of $\category{A}$,
     $f,f' \in \Mor (A,B)$ and $g,g' \in \Mor (B,C)$, then
     \[
       g \circ (f + f' ) = (g\circ f) + (g\circ f') \quad 
       \text{and} \quad
       (g + g') \circ f = (g \circ f) + (g'\circ f) \: .   
     \]
  \end{enumerate}
\end{definition}

\para Usually one denotes the set of morphism between objects $A$ and $B$ of a 
pre-additive category $\category{A}$ by $\Hom (A,B)$ instead of $\Mor (A,B)$.
We will follow this conention from now on. The zero element of $\Hom (A,B)$
will be denoted by $0_{(A,B)}$ or briefly by $0$, if no confusion can arise. 
In general, and as done already in the definition, we will abbreviate the group 
operation $+_{(A,B)}$ on $\Hom (A,B)$ by $+$ for clearity of exposition. 

A pre-additive structure on a category imposes quite a useful relation between 
finite products and coproducts of its objects, namely that they have to coincide 
when they exist. 

\begin{proposition}\label{thm:equivalence-of-finite-products-and-coproducts}
Let $\category{A}$ be a pre-additive category, and $A_1, \ldots , A_n$ a
finite family of objects in $\category{A}$.
\begin{enumerate}[label={\rmfamily (\arabic*)},leftmargin=*]
\item\label{ite:products}
 If $\prod_{l=1}^nA_l$ is a product with canonical projections 
 $p_k : \prod_{l=1}^n A_l \to A_k$, $k=1,\ldots , n$, then it is also a 
 coproduct where the canonical injections are given by the uniquely determined morphisms 
 $i_k : A_k \mapsto \prod_{l=1}^n A_l $ such that 
 \[
  p_l \circ i_k =
  \begin{cases}
    \id_{A_k}, & \text{if } k=l, \\
    0, & \text{else}.
  \end{cases}
 \]
 In addition, the equality
 \begin{equation}\label{eq:identity-product}
    \sum_{l=1}^n i_l \circ p_l = \id_{\prod_{l=1}^nA_l}
 \end{equation}
 holds true. 
 \item\label{ite:coproducts}
 If $\coprod_{l=1}^nA_l$ is a coproduct with canonical injections 
 $i_k : \coprod_{l=1}^n A_l \to A_k$, $k=1,\ldots , n$, then it is also a 
 product with canonical projections  given by the uniquely determined morphisms 
 $p_k : \coprod_{l=1}^n A_l \mapsto A_k$ such that 
 \[
  p_k \circ i_l =
  \begin{cases}
    \id_{A_k}, & \text{if } k=l, \\
    0, & \text{else}.
  \end{cases}
 \]
 In addition, the equality
 \begin{equation}\label{eq:identity-coproduct}
    \sum_{l=1}^n i_l \circ p_l = \id_{\coprod_{l=1}^nA_l}
 \end{equation}
 holds true. 
%\item
% If $\prod_{l=1}^nA_l$ is a product with canonical projections 
% $p_k : \prod_{l=1}^n A_l \to A_k$, $k=1,\ldots , n$, and 
% $\coprod_{l=1}^nA_l$ a coproduct with canonical injections 
% $i_k : \coprod_{l=1}^n A_l \to A_k$, $k=1,\ldots , n$, then there 
% exists a unique isomorphism 
% $\kappa: \coprod_{l=1}^nA_l \mapsto \prod_{l=1}^nA_l$ such that
% \[
%  p_k \circ \kappa \circ i_l =
%  \begin{cases}
%    \id_{A_k}, & \text{if } k=l, \\
%    0, & \text{else}.
%  \end{cases}
% \]
% The inverse of this isomorphism is given by 
% \[
%   \kappa^{-1} = \sum_{l=1}^n i_l \circ p_l \: .
% \] 
\end{enumerate}
\end{proposition}

\begin{proof}
Let us first show \ref{ite:products}. So assume that $\prod_{l=1}^nA_l$ is a product with 
canonical projections  $p_k$, and define the $i_k$ as in  \ref{ite:products}.
Then we have, for $k=1,\ldots, n$,
\[ 
 p_k \circ \Big( \sum_{l=1}^n i_l \circ p_l \Big) = 
 \sum_{l=1}^n p_k \circ i_l \circ p_l = p_k \: .
\]
By the universal property of the product, \Cref{eq:identity-product} follows.
Now let $ f_k : A_k \to X$, $k=1,\ldots ,n$, be a family of morphisms in $\category{A}$. 
Define $f: \prod_{l=1}^nA_l \to X$ by $f = \sum_{l=1}^n f_l \circ p_l$ and compute
\[ 
 f \circ i_k = \Big( \sum_{l=1}^n f_l \circ p_l \Big) \circ i_k = 
 \sum_{l=1}^n f_l \circ p_l \circ i_k = f_k \: .
\]
If $\tilde{f}: \prod_{l=1}^nA_l \to X$ is another morphism 
satisfying $\tilde{f} \circ i_k = f_k$ for all $i$, then 
\begin{equation*}
\begin{split}
  f - \tilde{f} \, &  
  = \big( f - \tilde{f} \big) \circ \Big( \sum_{l=1}^n i_l \circ p_l \Big)
  = \sum_{l=1}^n \big( f - \tilde{f} \big) \circ i_l \circ p_l = \\
  & = \sum_{l=1}^n \big( f - \tilde{f} \big) \circ i_l \circ p_l 
   = \sum_{l=1}^n \big( f_l - f_l \big) \circ p_l = 0 \: .
\end{split}
\end{equation*}
But this entails that $\prod_{l=1}^nA_l$ together with the morphisms $i_k$
fulfills the universal property of a coproduct of the family $(A_l)_{l=1}^n$.  

One shows \ref{ite:coproducts} by an analogous but dual argument.
\end{proof}

Since by the proposition the product and the coproduct of finitely many objects 
$A_k$, $k=1,\ldots,n$ in a pre-additive category $\category{A}$ coincide 
(up to canonical isomorphism), one denotes them by the same symbol, namely by
\[
  \bigoplus_{k=1}^n A_k ,
\]  
and calls the resulting object the \emph{direct sum} of the $A_k$. 
The proposition tells also that an initial or terminal object in $\category{A}$ 
has to be a zero object which we then denote by $0_\category{A}$ or $0$ if no 
confusion can arise. 

\begin{definition}
A pre-additive category $\category{A}$ is called \emph{additive}, if 
it has the following properties:
 \begin{enumerate}[label={({\sffamily A}\arabic*)},leftmargin=*]
  \setcounter{enumi}{-1}
  \item\label{axiom:Ab0}\hspace{0.4mm}
    $\category{A}$ has a zero object.
  \item\label{axiom:Ab1}\hspace{0.4mm}
    Every finite family of objects has a product.
  \vspace{-1mm}
  \end{enumerate}
  \begin{enumerate}[label={({\sffamily A}\arabic*)$^\circ$},leftmargin=*,resume]
  \addtocounter{enumi}{-1}
  \item\label{axiom:Ab1star} 
    Every finite family of objects has a coproduct.
  \vspace{-1mm}  
  \end{enumerate}
\end{definition}

\begin{example}
The category $\category{Ab}$ of abelian groups carries in a natural way the 
structure of an additive category. Likewise, if $R$ is a (unital) ring, the
category $R\text{-}\category{Mod}$ of $R$-left modules is additive. 
\end{example}

