% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum
% contributions by: 
%   Michael Martinez
%
\section{The compact-open topology on function spaces}\label{sec:compact-open-topology-function-spaces}

Let $X$ and $Y$ be topological spaces. We denote the set of all functions from $Y$ to $X$ 
by $X^Y$. This is the same thing as the direct product $\prod_Y X$ of $X$ over $Y$. 
The space of continuous functions $\shC(Y,X)$ sits in $X^Y$ so we can give 
$\shC(Y,X)$ the product topology induced by $X^Y$. This is the topology of 
\emph{pointwise convergence} and will not be useful for studying most function spaces. We will instead be 
interested in the \emph{compact open topology} which is the topology of 
\emph{uniform convergence on compact sets}.

\begin{definition}
Let $X$ and $Y$ be topological spaces. The \emph{compact open topology} on 
$\shC(Y,X)$ is the topology with subbasis given by the sets 
$\mathscr{V}(K,U)=\{f \in \shC (Y,X)|f(K) \subset U\}$ for $K \subset Y$ compact and 
$U \subset X$ open.
\end{definition}

\begin{definition}
A topology $\topology$ on $\shC(Y,X)$ is called \emph{admissable} if the 
evaluation map 
$e:\shC(Y,X) \times Y \rightarrow X$, $(f,y) \mapsto f(y)$ is continuous.
\end{definition}

\begin{proposition}
The compact open topology is coarser than any admissable topology on $\shC(Y,X)$.
\end{proposition}
\begin{proof}
Let $\topology$ be an admissable topology on $\shC(Y,X)$ so that the evaluation map $e:\shC(Y,X) \times Y \rightarrow X$ is continuous. Let $K\subset Y$ be compact, $U\subset X$ be open and $f\in T(K,U)$. We have to find $V\in O$ such that $f \in V \subset T(K,U)$. Let $k \in K$. Since $e$ is continuous and $U$ is an open neighborhood of $f(x)$, then there are open sets $W_k \subset Y$ and $V_k \subset C_O(Y,X)$ such that $k\in W_k$, $f(k)\in V_k$ amd $e(V_k \times W_k)\subset U$. Since $K$ is compact, there are $k_1,k_2,...,k_l \in K$ such that $K\subset \bigcup_{i=1}^l W_{k_i}$. 
Put $V:=\bigcap_{i=1}^l V_{k_i}$ so that $f \in V$ and $V$ is open in $O$. Now take $g \in V$ and let $k \in K$. Choose $i$ such that $k\in W_{k_i}$ and observe that $g\in W_{k_i}$ so that 
$$
g(k)=e(g,k) \in e(V_{k_i} \times W_{k_i} \subset U 
$$
Hense $g \in T(K,U)$
\end{proof}


\begin{theorem}
If $Y$ is locally compact, then the compact open topology on $\shC(Y,X)$ is admissable,
and it is the coarsets topology on $\shC(Y,X)$ with that property. 
\begin{proof}
We have to show that 
$$
e:\shC(Y,X) \times Y \rightarrow X \\
(f,y)\mapsto f(y)
$$
is continuous.
Since sets of the form $T(K,U)$ form a subbasis for the compact open topology, it suffices to show that for an open neighborhood $W \subset X$ of some $e(f,y)$, there is compact $K\subset Y$, open $U \subset X$ and open $V \subset Y$ such that $e(T(K,U) \times V) \subset W$ with $f \in T(K,U)$ and $y\in V$.
By assumption, and since $f$ is continuous, there is an open neighborhood $\tilde{W}$ of $y$ such that $f(\tilde{W}) \subset W$.  By local compactness, there is an open neighborhood $V\subset Y$ of $Y$ such that $y \in V \subset \bar{V} \subset \tilde{W}$ and $\bar{V}$ is compact. If we put $K:=\bar{V}$ and $U=W$, then $e(T(K,U) \times V) \subset W$ since for $f'\in T(K,U)$ and $y'\in V$, we have $e(f',y')=f'(y')\subset W$.
\end{proof}
\end{theorem}

Let $X,Y,Z$ be topological spaces. As sets, it is always true that $Z^{X \times Y} \cong Z^{Y^X}$ via the maps 
$$
\Phi:Z^{X \times Y} \rightarrow Z^{Y^X} \\
f \mapsto (x \mapsto (y \mapsto f(x,y)))
$$
and
$$
\Psi:Z^{Y^X} \rightarrow Z^{X \times Y} \\
g \mapsto ((x,y) \mapsto g(x)(y))
$$

\begin{theorem}[The exponential law]
If $Y$ is locally compact, then 
$$
\Phi(\shC(X \times Y),Z) \subset \shC(X,\shC(Y,Z))
$$
and 
$$
\Psi(\shC(X,\shC(Y,Z))) \subset (\shC(X \times Y),Z)
$$
\begin{proof}
For $f\in\shC(X \times Y, Z)$ and $x\in X$, we have to show that $\Phi(f)(x)\in\shC(Y,Z)$ and $\Phi(f)\in \shC(X,\shC(Y,Z))$. 
$\Phi(f)(x)(y)=f \circ i_x(y)=f(x,y)$.  Consider $T(K,U)$ for $K\subset Y$ compact and $U \subset X$ open. We need ot prove that the preimage $\Phi(f)^{-1}(T(K,U))$ is open in X. Let $x \in \Phi(f)^{-1}(T(K,U))$ so that $f(x,_) \in T(K,U)$. Hence for all $y \in K$, we have $f(x,y)\in U$. By the continuity of $f$, there are open neighborhoods $W_y$ of $x$ and $V_y$ of $y$ such that $f(W_y \times V_y)\subset U$. Since $K$ us compact, there are open sets $y_1,y_2, \ldots y_k \subset Y$ such that $K\subset V_{y_1} \cup V_{y_2} \cup \ldots \cup V_{y_k}$.  Put $W=W_{y_1} \cap W_{y_2} \cap \ldots \cap W_{y_k}$ so that $W$ is a neighborhood of $x$ and $\Phi(f)(W)\subset T(K,U)$.

Now we need to show for $g \in \shC(X,\shC(Y,Z))$ that $\Psi(g)\in \shC(X \times Y,Z)$. Let $g:X \times \shC(Y,Z)$ be continuous and assume that $U \subset Z$ be open. We have to show that $\Psi(g)^{-1}(U)$ is open. Take $(x,y)\in \Psi(g)^{-1}(U)$. Since $g$ is continuous, there is an open neighborhood $W$ of $y$ such that $g(x)(W) \subset U$. Since $Y$ is locally compact, there is an open $V \subset Y$ such that $y\in V \subset \bar{V} \subset W$ with $\bar{V}$ compact. Hence $g(x)(V)\subset g(x)(\bar{V})\subset U$. Thus $g(x)\in T(K,U)$ so there is an open neighborhood $O \subset X$ of $x$ such that $g(O) \subset T(\bar{V},U)$.  Therefore 
$$
\Psi(g)(O \times V) \subset g(O)(V) \subset g(O)(\bar{V}) \subset U
$$
\end{proof}
\end{theorem}

\begin{lemma}\label{cosubbasis}
The sets $(U^L)^K = T(K,T(L,U))$ with $K \subset X$ and $L \subset Y$ compact and $U \subset Z$ open form a subbasis for the compact open topology on $\shC(X,\shC(Y,Z))$.
\begin{proof}
Let $I$ be an index set $W_i \subset \shC(Y,Z)$ be open and $K\subset X$ be compact.
$$
T\left(K,\bigcup_I W_i\right)= 
\bigcup_{n \in \N^+} 
\bigcup_{\substack{K_1 \times \ldots \times K_n \subset K^n \\
			K_1\cup \ldots \cup K_n = K \\
			K_i = \bar{K_i} \forall i }}
\bigcup_{(i_1,\ldots,i_n)\in I^n}
\bigcap_{l=1}^n
T(K_{i_l},W_{i_l})
$$
Suppose $J$ is a finite set. then $T\left(K,\bigcap_{j \in J}W_j\right)=\bigcap_{j \in J}T(K,W_j)$. Sets of the form $T(L,U)$ with $L \subset Y$ compact and $U \subset Z$ open form a subbasis of $\shC(Y,Z)$, so if $W \subset \shC(Y,Z)$ is open, we have $W=\bigcup_{i\in I}\bigcap_{j \in J_i} T(L_{i_j},U_{i_j})$ so that 
$$
T(K,W)= 
\bigcup_{n \in \N^+} 
\bigcup_{\substack{K_1 \times \ldots \times K_n \subset K^n \\
			K_1\cup \ldots \cup K_n = K \\
			K_i = \bar{K_i} \forall i }}
\bigcup_{(i_1,\ldots,i_n)\in J^n}
\bigcap_{l=1}^n
\bigcap_{j \in J_{i_l}}
T(K_{i_l},T(L_{i_lj},U_{i_lj}))
$$

\end{proof}
\end{lemma}

\begin{theorem}
Let $X,Y,Z$ be topological spaces with $X$ and $Y$ Hausdorff and $Y$ locally compact. Then the natural isomorphism 
$$
\bar{\Phi}:\shC(X \times Y, Z) \rightarrow \shC(X, \shC(Y,Z))
$$
is a homeomorphism.
\begin{proof}
Let $f\in \shC(X \times Y, Z)$ and let $W\in \shC(X,\shC(Y,Z))$ be an open neighborhood of $\bar{\Phi}(f)$. By \ref{cosubbasis}, there is an open $U \subset Z$ and compact subsets $L \subset Y$ and $K \subset X$ such that $\bar{phi}(f)\in T(K,T(L,U)) \subset W$. $T(K \times L ,U)$ is open in $\shC(X \times Y, Z)$ and note that $f\in T(K \times L ,U)$ since for $(x,y)\in K \times L$, $\bar{\Phi}(f)(x)\in T(L,U)$ and $f(x,y)=\bar{\Phi}(f)(x)(y)\in U$.

Assume that $g\in T(K \times L, U)$. The $\bar{\Phi}(g)(x)(y)=g(x,y)=\in U$ so $\bar{\Phi}(g)(x)\in T(L,U)$ so $\bar{\Phi}(g)\in T(K,T(L,U))$, hence $\bar{\Phi}$ is continuous.
\marginpar{Rest of proof in email 9/27/10}
\end{proof}
\end{theorem}

%Student was assigned a paper on the category of compactly generated topological spaces.


