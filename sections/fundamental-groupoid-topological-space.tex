% Copyright 2017 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum
% 
\section{The fundamental groupoid of  a topological space}
\label{sec:fundamental-groupoid-topological-space}


\subsection*{The fundamental group}
\begin{definition}
  Let $(X,x_0)$ be a pointed topological space. The \emph{fundamental group}
  of $(X,x_0)$ is defined as
  \[
    \pi_1 (X,x_0) :=  
    \shC \big( (I,\partial I \}), (X,x_0)  \big) \big/\!\simeq_{_{\partial I}}
  \]
  which in other words is the set of homotopy classes relative $\partial I = \{ 0, 1\}$ of closed
  continuous paths $\gamma : I \to X$ based at $x_0$. 
\end{definition}

\para 
To turn $\pi_1(X,x_0)$ into a group need to define a binary operation 
\[ \ast :  \pi_1 (X,x_0) \times \pi_1 (X,x_0) \to  \pi_1 (X,x_0) \ .  \]
To this end let $\gamma, \varrho : I \to X$ be closed paths in $X$ based at $x_0$. 
Define their \emph{concatenation} $\varrho \ast \gamma : I \to X$ by 
\[
  \varrho \ast \gamma (s) := 
  \begin{cases}
    \gamma (2s) & \text{for } 0 \leq s \leq \frac 12, \\
    \varrho (2s-1)  & \text{for } \frac 12 \leq s \leq 1 .
  \end{cases}
\]
Then $\varrho \ast \gamma$ is again a closed path based at $x_0$. 
Its homotopy class depends only on the homotopy classes of $\gamma$ and $\varrho$. Namely, 
if $H : \gamma \simeq_{_{\partial I}} \widetilde{\gamma}$ and
$G : \varrho \simeq_{_{\partial I}} \widetilde{\varrho}$ are homotopies, then 
\[
  G \ast H : I \times I \to X, \quad (s,t) \mapsto 
  \begin{cases}
    H (2s ,t ) & \text{for } 0 \leq s \leq \frac 12, \\
    G (2s-1 , t)  & \text{for } \frac 12 \leq s \leq 1.
  \end{cases}
\] 
is a homotopy relative $\partial I$ from $\varrho \ast \gamma$ to $\widetilde{\varrho} \ast \widetilde{\gamma}$.
Hence the map 
\[ 
  \ast :  \pi_1 (X,x_0) \times \pi_1 (X,x_0) \to  \pi_1 (X,x_0),  \quad  
  [\varrho]_{_{\partial I}} \ast [\gamma]_{_{\partial I}} := [\varrho \ast \gamma]_{_{\partial I}} 
\]
is well-defined. 
\begin{theorem}
  The fundamental group $\pi_1 (X,x_0)$ of a based topological space $(X,x_0)$ is a group 
  with binary operation given by concatenation of paths. The homotopy class of 
  the constant path $e_{x_0}: I \to X$, $s\mapsto x_0$ acts as identity, and the inverse 
  of an element $[\gamma]_{_{\partial I}}$ is  the homotopy class of the path
  $\gamma^{-1}: I \to X$, $s \mapsto \gamma(1-s)$.  
\end{theorem}

\begin{proof}
  First we show associativity of $\ast$. Let $\gamma,\varrho, \eta :I \to X$ be closed paths in $X$ based
  at $x_0$. Then the paths $\eta\ast (\varrho \ast \gamma) : I \to X$ and 
  $( \eta\ast \varrho) \ast \gamma : I \to X$ are given by
  \[
    \big( \eta\ast (\varrho \ast \gamma)\big)   (s)  =
      \begin{cases}
       \gamma (4s) & \text{for } 0 \leq s \leq \frac 14, \\
       \varrho (4s -1 ) & \text{for } \frac 14 \leq s \leq \frac 12, \\
       \eta (2s -1) & \text{for } \frac 12 \leq s \leq 1, 
    \end{cases}
  \]
  and 
  \[
    \big( ( \eta\ast \varrho) \ast \gamma\big)   (s)  =
      \begin{cases}
       \gamma (2s) & \text{for } 0 \leq s \leq \frac 12, \\
       \varrho (4s - 2 ) & \text{for } \frac 12 \leq s \leq \frac 34, \\
       \eta ( 4s - 3 ) & \text{for } \frac 34 \leq s \leq 1.
    \end{cases}
  \]
  Now define the map $h : I \times I \to I $ by 
  \[
    h(s,t) = 
    \begin{cases}
      s (1+t) & \text{for } 0 \leq s \leq \frac 14, \\
      s + \frac t4  & \text{for } \frac 14 \leq s \leq \frac 12, \\
       s + \frac 12 (1-s)t  & \text{for } \frac 12 \leq s \leq 1, 
    \end{cases}
  \]
  and check that it is well-defined and continuous. Moreover, $h$ is a homotopy from 
  $h_0 = \id_I$ to the map 
  \[
    h_1 : I \to I , \quad s \mapsto 
     \begin{cases}
        2 s  & \text{for } 0 \leq s \leq \frac 14, \\
        s + \frac 14  & \text{for } \frac 14 \leq s \leq \frac 12, \\
        \frac 12 (s + 1)  & \text{for } \frac 12 \leq s \leq 1 .
     \end{cases}
  \]
  Hence $H : = \big( ( \eta\ast \varrho) \ast \gamma \big) \circ h : I \times I \to X$ is a homotopy
  from $H_0 = ( \eta\ast \varrho) \ast \gamma$ to $\big( (\eta\ast \varrho) \ast \gamma \big) \circ h_1$.
  But the latter map coincides with $\eta\ast ( \varrho \ast \gamma )$ since
  \[
    \big( (\eta\ast \varrho) \ast \gamma \big) \circ h_1 (s) =
    \begin{cases}
        \gamma (4 s)  & \text{for } 0 \leq s \leq \frac 14, \\
        \varrho ( 4s -1)   & \text{for } \frac 14 \leq s \leq \frac 12, \\
        \eta (2 s - 1)  & \text{for } \frac 12 \leq s \leq 1 .
     \end{cases}
  \]
  Hence $\eta\ast ( \varrho \ast \gamma )$ and $(\eta\ast \varrho) \ast \gamma$ are homotopic 
  paths which proves associativity of the operation $\ast$ on $\pi_1(X,x_0)$. 

  Next consider the concatenations $\gamma \ast e_{x_0}$ and $e_{x_0} \ast \gamma$.  
  Then
  \[
    H : I \times I \to X, \quad (s,t) \mapsto 
    \begin{cases}
      \gamma ((1-t)s ) & \text{for } 0 \leq s \leq \frac 12, \\
      \gamma (s + t (s-1)))  & \text{for } \frac 12 \leq s \leq 1.
    \end{cases}
  \] 
  is a homotopy between $H_0 = \gamma$ and $H_1 = \gamma \ast e_{x_0}$, and 
  \[
    G : I \times I \to X, \quad (s,t) \mapsto 
  \begin{cases}
    \gamma (s (1+t) ) & \text{for } 0 \leq s \leq \frac 12, \\
    \gamma ( s +t -st )  & \text{for } \frac 12 \leq s \leq 1.
  \end{cases}
  \]
  a homotopy between $G_0 = \gamma$ and $G_1 = e_{x_0} \ast \gamma$. Hence $e_{x_0}$ is the 
  identity element of $\pi_1 (X,x_0)$. 

  Finally consider the concatenation 
  \[
   \gamma^{-1} \ast \gamma : I \to X,\quad s \mapsto
    \begin{cases}
      \gamma (2s ) & \text{for } 0 \leq s \leq \frac 12, \\
      \gamma (2 (1-s))  & \text{for } \frac 12 \leq s \leq 1.
    \end{cases}
  \]
  Then
  \[
    H : I \times I \to X, \quad (s,t) \mapsto 
    \begin{cases}
      \gamma (2(1-t)s ) & \text{for } 0 \leq s \leq \frac 12, \\
      \gamma (2 (1-s)(1-t))  & \text{for } \frac 12 \leq s \leq 1.
    \end{cases}
  \] 
  is a homotopy between $H_0 = \gamma^{-1} \ast \gamma$ and $H_1 = e_{x_0}$. Since by definition
  $\big( \gamma^{-1}\big)^{-1} = \gamma$, the paths  $\gamma \ast \gamma^{-1}$ and $e_{x_0}$
  are homotopic as well. So $\gamma^{-1}$ is the inverse of $\gamma$, and $\pi_1 (X,x_0)$ is a group 
  as claimed. 
\end{proof}
