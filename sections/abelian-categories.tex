% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum 
%
\section{Abelian categories}\label{sec:abelian-categories}

\begin{definition}
 By an \emph{abelian category} one understands an additive category $\category{A}$
 which fulfills the following axioms by Grothendieck:
 \begin{enumerate}[label={({\sffamily AB}\arabic*)},leftmargin=*]
 \item\label{axiom:AB1}
  Every morphism has a kernel and a cokernel. 
 \item\label{axiom:AB2}
  For every morphism $f$ the induced canonical morphism $\coim f \to \image f$ is an isomorphism.
 \vspace{-1mm}
 \end{enumerate}
\end{definition}


\begin{proposition}
 Assume that $\category{A}$ is an abelian category, and let
 \begin{equation}\label{dia:square-abelian-category}
 \begin{tikzcd}
   X \arrow[r,"f"]\arrow[d,"g"'] & A \arrow[d,"r"]\\
   B \arrow[r,"s"'] & Y 
 \end{tikzcd}
 \end{equation}
 be a commutative diagram in $\category{A}$.
 \begin{enumerate}[label={\rmfamily (\arabic*)},leftmargin=*]
 \item \label{ite:pullback-sequence} 
 The diagram is cartesian if and only if the sequence 
 \begin{equation}\label{seq:pullback-sequence} 
  0 \xrightarrow{\hspace{3em}}    X \xrightarrow[\hspace{3em}]{i_1f+i_2g}    
  A \oplus B \xrightarrow[\hspace{3em}]{rp_1 - sp_2}   Y  
 \end{equation}
 is exact.
 \item\label{ite:pushout-sequence}
 The diagram is  cocartesian, if and only if 
 \begin{equation}\label{seq:pushout-sequence}
  X \xrightarrow[\hspace{3em}]{i_1f - i_2g}   
  A \oplus B \xrightarrow[\hspace{3em}]{rp_1 +  sp_2}   Y  \xrightarrow{\hspace{3em}}  0
 \end{equation}
 is exact. 
 \item\label{ite:kernels-cartesian-square-with-epis-abelian-category}
 If the diagram is cartesian, and  $s$ an epimorphism, then the diagram is even bicartesian, and 
 $f$ is an epimorphism, too. Moreover, one obtains in this case a commutative diagram with exact rows
 \begin{equation}\label{dia:kernels-cartesian-square-with-epis-abelian-category}
 \begin{tikzcd}
   0 \arrow[r] & \ker s \arrow[r]\arrow[equal]{d} & X \arrow[r,"f"]\arrow[d,"g"'] & A \arrow[d,"r"] \arrow[r]& 0\\
   0 \arrow[r] & \ker s \arrow[r] & B \arrow[r,"s"'] & Y  \arrow[r]& 0 \: .
 \end{tikzcd}
 \end{equation} 
 In particular this means that the kernel of $s$ factors through $g$ then.
 \item\label{ite:cokernels-cocartesian-square-with-monos-abelian-category}
 If the diagram is cocartesian, and  $f$ a  monomorphism, then the diagram is even bicartesian, and
 $s$ is a monomorphism, too.  Moreover, one obtains in this case a commutative diagram with exact rows
  \begin{equation}\label{dia:cokernels-cocartesian-square-with-monos-abelian-category}
 \begin{tikzcd}
   0 \arrow[r] & X \arrow[r,"f"]\arrow[d,"g"'] & A \arrow[d,"r"] \arrow[r] &  \coker f \arrow[r]\arrow[equal]{d} & 0 \\
   0 \arrow[r] & B \arrow[r,"s"'] & Y \:  \arrow[r] & \coker f \arrow[r] & 0 .
 \end{tikzcd}
 \end{equation} 
 In particular this means that the cokernel of $f$ factors through $r$ then.
 \end{enumerate}
\end{proposition}
\begin{proof}
 To prove \ref{ite:pullback-sequence}, consider the sequence 
 \begin{equation}
  0 \xrightarrow{\hspace{3em}}    K \xrightarrow[\hspace{3em}]{k}    
  A \oplus B \xrightarrow[\hspace{3em}]{rp_1 - sp_2}  Y,
 \end{equation}
 where $k$ is the kernel of $rp_1 - sp_2$. Given a commutative diagram 
 \begin{equation*}
 \begin{tikzcd}
   P \arrow[r,"l"]\arrow[d,"m"'] & A \arrow[d,"r"]\\
   B \arrow[r,"s"'] & Y,
 \end{tikzcd}
 \end{equation*}
 the morphism $P \xrightarrow{i_1l + i_2m} A\oplus B$ must then factor through $k$ in a unique way. Since the 
 diagram
 \begin{equation*}
 \begin{tikzcd}
   K \arrow[r,"p_1k"]\arrow[d,"p_2k"'] & A \arrow[d,"r"]\\
   B \arrow[r,"s"'] & Y
 \end{tikzcd}
 \end{equation*}
 commutes as well, this implies that \eqref{dia:square-abelian-category} is cartesian if and only if 
 the sequence \eqref{seq:pullback-sequence} is exact. 
 
 Next let us show \ref{ite:kernels-cartesian-square-with-epis-abelian-category}. So assume that the diagram \eqref{dia:square-abelian-category}
 is cartesian and that $s$ is epic. 
 Then $rp_1 - sp_2$ must be epic as well, since $(rp_1 - sp_2)i_2 = - s$.
 So both sequences  \eqref{seq:pullback-sequence} and \eqref{seq:pushout-sequence} are exact, and the diagram is bicartesian.
 Now assume that $hf =0$ for some morphism $h$. Then $f = p_1 k$, where $k=i_1f+i_2g$ is monic by  \ref{ite:pullback-sequence}.
 Since $hp_1k = 0$, the morphism $hp_1$ factors through the cokernel of $k$ which is $rp_1 +  sp_2$. Hence
 $hp_1 = h'(rp_1 +  sp_2)$ for some $h'$. One then obtains
 \[
   0= hp_1i_2 = h'(rp_1 +  sp_2)i_1 = h'r \: .
 \]
 By assumption, $r$ is epic, hence $h'=0$. But then $hp_1=0$, which entails $h=hp_1i_i =0$. Therefore $f$ must be epic as well.
 
 Now consider $l : \ker s \to B$, the kernel of $s$. Since $sl = 0 = r0$, and since the diagram  \eqref{dia:square-abelian-category}
 is assumed to be cartesian, there exists a unique 
 $l' : \ker s \to X$ such that $ gl' = l$ and $fl'=0$. As a  kernel, $l$ is monic, hence so is $l'$. It remains to show that
 $l'$ is the kernel of $f$. To this end assume $fj =0$ for some morphism $j$. Because $sgj = rfj=0$, $gj$ factors through the 
 kernel of $s$, hence $gj=l j' = gl'j'$, and $0= sgj = sgl'j'$. On the other hand, $rfj = 0 = sgl'j' =rfl'j'$.
 By the universal property of the pullback one obtains $j =l'j'$. Since $j'$ is monic, $j'$ is uniquely determined by $j$,
 so $l'$ is the kernel of $f$. 
 
 Statments \ref{ite:pushout-sequence} and \ref {ite:cokernels-cocartesian-square-with-monos-abelian-category} follow by dualization. 
\end{proof}