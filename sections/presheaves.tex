% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum 
%
\section{Presheaves}
\label{sec:presheaves}
%
\subsection*{The category of open sets of a topological space}
\para 
Before we define presheaves and sheaves on  a topological space 
$(X,\topology)$, we briefly introduce the category 
$\category{Ouv}(X)$ of open sets of $(X,\topology)$. 
By definition, its object class coincides with the set of open sets 
$\topology$, so $\category{Ouv}(X)$ is in particular a small category.
For two open  $U,V \subset X$ the morphism set $\Mor_{\category{Ouv}(X)} (U,V)$ 
is defined to be empty in case $ U\not\subset V$
and consists of the canonical (identical) embedding 
$i^V_U: U \hookrightarrow V$ when $U \subset V$. Obviously,
the identity map $i^U_U$ is then a morphism for every open $U\subset X$,
and the composition of morphisms in this category is given by 
\[
  i^W_V \circ i^V_U = i^W_U : U \hookrightarrow W
  \quad \text{for } U,V,W \in \topology \text{ with } U\subset V \subset W.
\]
This observation entails that $\category{Ouv}(X)$ is a category indeed; it is 
called the \emph{category of open sets} on the topological space 
$(X,\topology)$.

\begin{remarks}
\begin{environmentlist}
\item    
  The topology $\topology$ carries a natural partial order given by 
  set-theoretic inclusion, so becomes a poset. The corresponding
  category structure from \Cref{ex:category-posets} is canonically isomorphic
  to $\category{Ouv}(X)$.  
\item
  The notation $\category{Ouv}$ stems from the French word `ouvert' for 
  `open'.
\end{environmentlist}
\end{remarks}

\begin{proposition}
  Let $(X,\topology)$ be a topological space. Then the category $\category{Ouv}(X)$ has the following 
  properties.
  \begin{romanlist}
  \item 
    The empty set $\emptyset$ is an initial object in $\category{Ouv}(X)$, the
    full set $X$ a final object.
  \item
    Fibered products exist in $\category{Ouv}(X)$. 
    More precisely, if $i^W_U :U \hookrightarrow W$ and
    $i^W_V :V \hookrightarrow W$
    are two morphisms in  $\category{Ouv}(X)$, the fibered product 
    $U \times_W V $ is given by the open set $U\cap V$ together with the canonical embeddings 
    $i^U_{U\cap V} : U \cap V \hookrightarrow U$ and
    $i^V_{U\cap V} : U \cap V\hookrightarrow V$. 
  \item 
    Directed colimits exist in  $\category{Ouv}(X)$. 
  \item 
    Finite limits exist in  $\category{Ouv}(X)$.
  \end{romanlist}
\end{proposition}
\begin{proof}
\begin{adromanlist}
\item
    The first claim  follows from the fact that $\emptyset$ is contained in every element of $\topology$ and that every element  of $\topology$ 
    is contained in $X$.
\item
    Assume to be given $O \in \topology$ such that the following diagram commutes: 
    \begin{center}
    \begin{tikzcd}
        O \arrow[d] \arrow[r] & V \arrow[d] \\
        U \arrow[r] & W % \rlap{\ .}
    \end{tikzcd}  
    \end{center} 
    Then $O \subset U\cap V$, and the  diagram
    \begin{center}
    \begin{tikzcd}
        O \arrow[rd] \arrow[rrd] \arrow[rdd] & & \\
        & U\cap V \arrow[d] \arrow[r] & V \arrow[d] \\
        & U \arrow[r] & W % \rlap{\ .}
    \end{tikzcd}
    \end{center}
  commutes with morphisms unique.
\item 
   Assume that $(I,\leq)$ is an upward directed set and
   $(U_i)_{i\in I}$ a directed system in $\category{Ouv}(X)$.
\end{adromanlist}
\end{proof}

\subsection*{The category of presheaves on a topological space}

\begin{definition}
  Let $\category{C}$ denote a category and  $(X,\topology)$ be a topological space.
  By a \emph{presheaf} on $X$ with \emph{values} in $\category{C}$ one understands a contravariant functor
  $\mathscr{F} : \category{Ouv} (X)\to \category{C}$. A \emph{morphism} between two presheaves
  $\mathscr{F}: \category{Ouv} (X)\to \category{C}$ and
  $\mathscr{G}: \category{Ouv} (X)\to \category{C}$  is a natural transformation
  $\eta : \mathscr{F} \to \mathscr{G}$.
  The functor category $\category{Func} \big(\category{Ouv} (X)^\textup{op}, \category{C}\big)$
  is called the  \emph{category of presheaves} on $X$ with \emph{values} in $\category{C}$.
  It is denoted by $\category{PSh}_{\category{C}}(X)$.
  By definition, its objects are the presheaves on $X$, its morphisms are given by 
  morphisms of presheaves.
  When the underlying category is the category of sets we just write
  $\category{PSh} (X)$ instead of $\category{PSh}_{\category{Ens}}(X)$.
  
\end{definition} 

\subsection*{Types of algebraic structures}
In general, the category $\category{C}$ in which a presheaf $\mathscr{F}$ takes its values comes equipped with
some type of additional, usually algebraic structure. Not only is this suggested by examples, also the theory
of presheaves becomes richer by that. We want to specify the kind of categories
which presheaves are allowed to take values in. 

\begin{definition}[]
  By a \emph{type of algebraic structure}
\end{definition}




\subsection*{The \'etal\'e space of a presheaf}
\para
Let $\mathscr{F}$ be a presheaf on the topological space $(X,\topology)$ with values in the category $\category{C}$.
We assume that directed colimits exist in $\category{C}$. 
Given a point $x\in X$, the system $\mathscr{N}_x^\circ$ of open neighborhoods of $x$ is (upward) directed by defining
$V \leq U$ for $U,V\in \mathscr{N}_x^\circ$ if $U\subset V$. Since $\mathscr{F}$ is a contravariant functor
from $\category{Ouv} (X)$ to $\category{C}$, the diagram $(\mathscr{F} (U))_{U\in \mathscr{N}_x^\circ}$ is a directed system
in $\category{C}$, hence has a colimit by assumption. We write
\[
   \mathscr{F}_x := \colim_{\, U\in\mathscr{N}^\circ_x} \mathscr{F} (U) 
\]
and call  $\mathscr{F}_x$ the \emph{stalk} of $\mathscr{F}$ at $x$.  For every open neighborhood  $U$ of $x$ we denote the
image of a section $s\in \mathscr{F}(U)$ in the stalk  $\mathscr{F}_x$ by $[s]_x$ or $s_x$ and  call it the \emph{germ} of $s$
at $x$. When denoting a germ at $x$ by $s_x$ we therefore always silently assume that $s$ is an element of some
section space $\mathscr{F}(U)$ over an open neighborhood  $U$ of $x$ and that that $s$ is a representative of the germ
considered. Next we study the disjoint union of the stalks
\[
  \Etsp (\mathscr{F}) := \bigsqcup_{x\in X}  \mathscr{F}_x := \bigcup_{x\in X}  \mathscr{F}_x \times \{ x\} \ .
\]
By construction, we have a natural projection map $\sheafify{\pi} : \Etsp (\mathscr{F}) \to X$ which maps an element
$(s_x,x) \in \Etsp (\mathscr{F})$ to the footpoint $x\in X$. Given a section $s\in \mathscr{F}(U)$ defined over an open
set $U\subset X$ we define a map $\sheafify{s} : U \to \Etsp (\mathscr{F})$ by
\[
  \sheafify{s} (x) = (s_x,x) \quad \text{for all } x \in U \ .
\]
Now we make the following observation. 
\begin{propanddef}
  Assume that $(X,\topology)$ is a topological space and  $\category{C}$ a category in which directed colimits exist.
  Let $\mathscr{F}$ be a presheaf on $X$ with values in $\category{C}$. Then the set  $\sheafify{\mathscr{B}}$ consisting of all 
  sets of the form $\sheafify{s} (U)$ with $U\subset X$ open and $s \in \mathscr{F}(U) $
  is the basis of a topology $\sheafify{\topology}$ on $\Etsp (\mathscr{F})$.
  One calls the topological space $\big(\Etsp (\mathscr{F}),\sheafify{\topology}\big)$ the
  \emph{\'etal\'e space} of the presheaf $\mathscr{F}$.
\end{propanddef}

\begin{proof}
  First observe that $\mathscr{B}$ covers $\Etsp (\mathscr{F})$ since every element of $\Etsp (\mathscr{F})$
  is of the form $(s_x,x)$ for  some open $U\subset X$ and section $s\in \mathscr{F} (U)$ 
  and since $(s_x,x)$ is contained in the basis element $\sheafify{s} (U)$.

  It remains to show that the intersection of two elements $\sheafify{s} (U)$ and $\sheafify{t} (V)$  of
  $\sheafify{\mathscr{B}}$ can be written as the union of elements of  $\sheafify{\mathscr{B}}$. So let 
  $(u_x,x) \in \sheafify{s} (U) \cap \sheafify{t} (V)$. The germ $u_x$ is represented by some section
  $u \in \mathscr{F}(W)$ over some open neighborhood $W$ of $x$. By assumption and since both $\sheafify{s}$ and $\sheafify{t}$
  are sections of $\sheafify{\pi}$ one obtains 
  \[
     (u_x,x) = \sheafify{s} (x) = \sheafify{t} (x) \ .
  \]
  Hence there exists an open neighborhood $O\subset U\cap V\cap W$ of  $x$ such that
  \[
     u|_O = s|_O = t|_O \ .
  \]
  But that entails
  \[
    (u_x,x) \in  \sheafify{u}(O) = \sheafify{s}(O) = \sheafify{t}(O) \subset \sheafify{s} (U) \cap \sheafify{t} (V) \ .
  \]
  This proves the claim. 
\end{proof}

\begin{corollary}
  The canoncial projection $\sheafify{\pi} : \Etsp (\mathscr{F})\to X$ of a presheaf $\mathscr{F}$ on the topological space
  $(X,\topology)$ is an \'etale map or in other words  a local homeomorphism.
  More precisely, for every  open $U\subset X$ and every $s\in \mathscr{F} (U)$ the section
  $\sheafify{s} : U \to \Etsp (\mathscr{F})$ is a homeomorphism onto its image. 
\end{corollary}

\begin{proof}
  We first prove that $\sheafify{\pi}$ is continuous. So let $U\subset X$ open. Then observe that
  \[
    (\sheafify{\pi})^{-1} (U) = \bigcup_{x\in U}  \mathscr{F}_x \times \{ x\} =
    \bigcup_{x\in U} \bigcup_{V\in \mathscr{N}^\circ_x\atop V\subset U} \bigcup_{s\in \mathscr{F} (V)} \{ (s_x,x)  \} =
    \bigcup_{V \in \category{Ouv}(X) \atop V\subset U} \bigcup_{s\in \mathscr{F} (V)} \sheafify{s}(V) \ .
  \]
  But this is open by the preceding proposition, hence  $\sheafify{\pi}$ is continuous.

  Next we show continuity of $\sheafify{s}$. To this end it suffices to verify that
  $(\sheafify{s})^{-1} \big(\sheafify{t} (V)\big)$ is open in $U$ for every open $V\subset X$ and $t\in \mathscr{F}(V)$.
  Now compute
  \begin{equation*}
    \begin{split} 
    (\sheafify{s})^{-1} \big(\sheafify{t} (V)\big) \: & = \big\{ x \in U \bigmid \sheafify{s} (x) \in \sheafify{t} (V) \big\}
    = \big\{ x \in U \bigmid x \in V \:\&\: \sheafify{s} (x) = \sheafify{t}  (x) \big\} \\
    & =  \big\{ x \in U \cap V \bigmid s_x = t_x \big\} \ .  
    \end{split} 
  \end{equation*}
  The right hand side is  open since if it contains $y$, then the sections $s$ and $t$ coincide
  on an open neighborhood $O\subset U\cap V$ of $y$ which means that $O\subset \big\{ x \in U \cap V \bigmid s_x = t_x \big\}$.
  
  Finally observe that $\sheafify{s}$ is a section of $\sheafify{\pi}$ and that
  \[
    \sheafify{s} \circ  \sheafify{\pi}|_{\sheafify{s}(U)} = \id_{\sheafify{s}(U)}
  \]
  which implies that $\sheafify{s}$ is a homeomorphism onto its image. 
  The claim now follows from the fact that the sets $\sheafify{s}(U)$ with $U\in \category{Ouv} (X)$ and
  $s\in \mathscr{F} (U)$ cover $\Etsp (\mathscr{F})$. 
\end{proof}