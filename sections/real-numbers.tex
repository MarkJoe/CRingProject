% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum 
%
\section{The real numbers}
\label{sec:real-numbers}
%
\subsection*{Complete ordered fields}
%
\para 
  Unless some ambiguity could arise we will from now on denote a 
  an ordered field $(\fldF, +, \cdot , 0, 1, \leq)$ simply by the 
  symbol $\fldF$. 

\begin{definition}
  An ordered field $\fldF$ is called \emph{Dedekind complete} if every set bounded above has 
  a least upper bound. 
\end{definition}

\begin{lemma}
  An ordered field  $\fldF$ is Dedekind complete if and only if every set bounded below has 
  a greatest lower bound. 
\end{lemma}

\begin{proof}
  The claim is an immediate consequence of the fact that multiplication by $-1$ is a 
  strictly order reversing automorphism of the abelian group $(\fldF,+,0)$. 
\end{proof}

\begin{proposition}
  A Dedekind complete ordered field $\fldF$ is archimedean. 
\end{proposition}

\begin{proof}
  Assume that $\fldF$ is non-archimedean which in other words means that $\N$ is a bounded subset 
  of $\fldF$. By Dedekind completeness there exists a least upper bound $M$ of $\N$. 
  Since $M-1 < M$ the element $M-1$ is not an upper bound of $\N$, hence there exists an $n\in \N$ such 
  that $ M-1  < n$. But then $M < n +1$ which is a contradiction. Therefore $\fldF$ is archimedean.
\end{proof}

\begin{example}
  The converse of the proposition does not hold true in general. 
  For example the ordered field of rational numbers is archimedean but not Dedekind complete. 
  To prove this consider the set 
  $X = \{ x \in \Q \mid x^2 \leq 2 \}$. Since $ |x|^2 = x^2 \leq 2 < 2^2$ for $x \in X$,
  the estimate $|x| < 2$ holds true for all $x \in X$ by \Cref{thm:properties-order-ordered-integral-domain} \ref{ite:montonony-square}.
  Hence $X$ is a bounded subset of $\Q$. But $X$ does not have a least upper bound in $\Q$. 
  To prove this we will lead the assumption that $X$ has a least upper bound in $\Q$ 
  to a contradiction. So assume that $b$ is rational and a least upper bound of $X$. 
  First note that $b >1$ since $1\in X$. If $b^2 < 2$ choose $n\in \gzN$ so that 
  $2b+1 < n \cdot \left( 2 -b^2 \right) $. Then $\left(b + \frac 1n \right)^2 \leq b^2 + \frac{2b+1}{n} < 2$   
  which contradicts the assumption that   $b$ is an upper bound of $X$. Hence $b^2 \geq 2$. 
  Assume that $b^2 > 2$ and put $c = \frac 12 \left( b + \frac 2 b \right)$. 
  Then $c^2  -2 = \frac{1}{4} \left( b - \frac 2b \right)^2 \geq 0 $, hence 
  $c$ is an upper bound of $X$. But $b -c = \frac{1}{2b} \left( b^2 -2\right) >0 $ 
  which contradicts that $b$ is the least upper bound of $X$. So $b^2 = 2$. 
  But that contradicts \Cref{thm:non-existence-rational-root-negative-one-prime-number},
  hence $\Q$ is not Dedekind complete.
\end{example}

\begin{definition}
  By  a \emph{Dedekind cut} or shortly a \emph{cut} in an archimedean ordered field $\fldF$ one 
  understand a pair $(A,B)$ of subsets $A,B \subset \fldF$ such that the following holds true
  \begin{axiomlist}[DC]
  \item\label{axiom:dedekind-cut-one}  
    The sets  $A,B $ form a partition of $\fldF$ that means $\fldF = A \cup B$. 
  \item\label{axiom:dedekind-cut-two} 
    The set $A$ does not have a greatest element. 
  \item\label{axiom:dedekind-cut-three} 
    For all elements $a \in A$ and $b\in B$ one has $a < b$. 
  \end{axiomlist}
  A cut $(A,B)$ of $\fldF$ is called a \emph{gap} if $B$ does not have  a least element. 
\end{definition}

\begin{example}
  Let $\fldF$ be archimedean ordered, $x \in \fldF$ and put $A = \{ y\in \fldF \mid y < x \} $ and 
  $B= \{ z\in \fldF \mid z \geq x \} $. Then $(A,B)$ is a Dedekind cut which is not a gap. 
\end{example}

\begin{proposition}
Let $(A,B)$ be a   Dedekind cut in the archimedean ordered field $\fldF$. 
Then the following holds true:
\begin{romanlist}
\item\label{ite:left-inclusive} 
  If $a,x \in \fldF$ satisfy  $a \in A$ and $x \leq a$, then $x \in A$.
\item\label{ite:right-inclusive} 
  If $b, y\in \fldF$ satisfy  $b\in B$ and $b \leq y$, then $y \in B$.
\item \label{ite:lower-open} 
  The lower part $A$ is always open in $\fldF$ and satisfies $\interior{\closure{A}}= A$.
\item \label{ite:upper-closed} 
  The upper part $B$ is always closed in $\fldF$ and satisfies $\closure{\interior{B}}= B$.
\item \label{ite:gap-open-closed} 
  If $(A,B)$ is a gap, then $A$ and $B$ are both open and closed. 
\end{romanlist}
\end{proposition}

\begin{proof} 
  Properties \ref{ite:left-inclusive} and \ref{ite:right-inclusive} are immediate consequences of \ref{axiom:dedekind-cut-one} and \ref{axiom:dedekind-cut-three}. 

  To verify that the lower part $A$ is open let $x \in A$. Since $A$ does not 
  have a greatest element there exists a $a\in A$ with $x < a$. 
  Since $\fldF$ is archimedean there exists $n\in \N$ such that 
  $a-x < n$. Hence $a-n < x < a$. Since every element $y \in \fldF$ with 
  $y < a$ is an element of $A$ by \ref{ite:left-inclusive}, the interval
  $\openint{a-n,a}$ is contained in $A$ and contains $x$. So $A$ is open in 
  the order  topology. Its complement $B$ therefore is closed. 
  
  The relation $A \subset \closure{A}$  entails 
  $A = \interior{A} \subset \interior{\closure{A}}$.
  To verify \ref{ite:upper-closed} it therefore remains to prove that 
  $\interior{\closure{A}}\subset A$. Assume that 
  $x \in\interior{\closure{A}}$. Then there exist
  $a,b \in \fldF$ such that $x\in \openint{a,b}\subset\interior{\closure{A}}$.
  But this means that every open neighborhood of $b$ contains a point of 
  $\closure{A}$ hence meets $A$. Therefore $b \in \closure{A}$. 
  Choose $c \in \fldF$ such that $x < c <b$. Then $\openint{c,b+1}$ is
  an open neighborhood  of $b$, so contains some element $d\in A$. 
  Since $x < d$, the relation $x \in A$ follows by \ref{ite:left-inclusive}
  and \ref{ite:upper-closed} is proved.
  
  Claim \ref{ite:upper-closed} follows from the equality
  \[
    \closure{\interior{B}} = \fldF \, \setminus \, \interior{\closure{A}} 
    =   \fldF \, \setminus \, A = B \ .
  \]

  To prove \ref{ite:gap-open-closed} is suffices to show that 
  $B$ is open. Let $y$ be an element of $B$. Since by assumption $B$ does 
  not have a least element there exists $b \in B$ with $b < y$. 
  Choose $n\in \N$ such that $y -b < n$. Then 
  $y \in \openint{b,b+n} \subset B$, so $B$ is open and the last claim is verified. 
\end{proof}

\begin{proposition}
  For an archimedean ordered field $(\fldF, +, \cdot , 0, 1, \leq)$ the following properties 
  are equivalent:
  \begin{romanlist}
   \item\label{ite:dedekind-completeness}
    The ordered field $\fldF$ is Dedekind complete. 
   \item\label{ite:no-gap-dedekind-cuts}
    No Dedekind cut in $\fldF$ is a gap. 
  \item\label{ite:nested-interval-property}
    The \emph{nested interval property} holds for $\fldF$ which means 
    that for all sequences $\big(\closedint{a_n,b_n}\big)_{n\in \N}$ of intervals fulfilling
    $\closedint{a_{n+1},b_{n+1}} \subset \closedint{a_n,b_n}$ for all $n \in \N$ the intersection
    $\bigcap_{n\in \N}\closedint{a_n,b_n}$ is non-empty.
  \item\label{ite:cauchy-completeness}
    The archimedean ordered field $\fldF$ is \emph{Cauchy complete} that is  
    every Cauchy sequence in $\fldF$ converges.
  \end{romanlist}
\end{proposition}

\begin{proof}
  
\end{proof}
%
\subsection*{Definition and uniqueness of real number fields}
%
\begin{definition}
  By a \emph{real number field} one understands a complete archimedean ordered field.
\end{definition} 

\para 
As we will show in this section all real number fields coincide up to isomorphism.
Moreover, there exists a real number field. The proof will be broken up in a 
a uniqueness part and an existence part. For the existence we need the notion of a morphism of 
(ordered) fields which will be introduced next. 
Existence will be shown in two ways, first in the way real numbers were constructed by R.~Dedekind \cite{}, 
then by constructung the complete hull of $\Q$ via equivalence classes of Cauchy sequences. 

\begin{definition}
  A \emph{field homomorphism} or shortly a \emph{homorphism} between two fields $\fldK$ and  $\fldF$ is a ring homomorphism $f: \fldK \to \fldF$.
  If both  fields are ordered, an order preserving field homomorphism $f: \fldK \to \fldF$ is called a \emph{morphism of ordered fields}. 
\end{definition}

\begin{proposition}
  Fields with field homomorphisms as morphisms form a  category denoted $\category{Fld}$. 
  Ordered fields together with  order preserving field homomorphism as morphisms are a  subcategory $\category{OFld}$. 
  The real number fields form a full subcategory of $\category{OFld}$. 
\end{proposition}

\begin{proof}
  Obviously, the identity map $\id_\fldK$ of a field $\fldK$ is  a morphism. 
  If $\fldK$ is ordered, the identity map is also order preserving.
  Moreover, the composition of field morphisms is a morphism as well. 
  If both morphisms are order preserving the composition is so as well. The claim follows.
\end{proof}

\begin{proposition}
  Every field homomorphism $f: \fldK \to \fldF$ is injective. 
\end{proposition}

\begin{proof}
  Let $x,y \in \fldK$ and assume that $f(x) =f(y)$. Then $f(x-y) = 0$.
  If $x\neq y$, the difference $x-y$ has an inverse $z$.  
  But that implies
  \[
    1 = f(1) = f\big( z(x-y) \big) = f(z) f(x-y) = 0 
  \]
  which is impossible. So $x=y$ and $f$ is injective. 
\end{proof}

\begin{theorem}
  Let $\fldK$ and $\fldF$ be real number fields. 
  Then there exists an order preserving field homomorphism
   $f:\fldK\to\fldF$.  Every such morphism is an isomorphism.
\end{theorem}

\begin{proof}
  By the preceding proposition we only need to show that $f$ is surjective. 
\end{proof}

%
\subsection*{Real numbers \`a la Dedekind}
%


%
\subsection*{Cauchy completion of the field of rational numbers}
%
\para Let $\Q^\N_\textup{Cauchy}$ denote the set of all Cauchy sequences in $\Q$. Addition and multiplication 
can be extended pointwise from $\Q$ to $\Q^\N_\textup{Cauchy}$ as follows:
\begin{equation*}
\begin{split}
  + : & \: \Q^\N_\textup{Cauchy} \times \Q^\N_\textup{Cauchy} \to \Q^\N_\textup{Cauchy}, \quad 
  \Big( (x_n)_{n\in \N} , (y_n)_{n\in \N} \Big) \mapsto  (x_n+ y_n)_{n\in \N} \\
  \cdot : & \: \Q^\N_\textup{Cauchy} \times \Q^\N_\textup{Cauchy} \to \Q^\N_\textup{Cauchy}, \quad 
  \Big( (x_n)_{n\in \N} , (y_n)_{n\in \N} \Big) \mapsto  (x_n \cdot y_n)_{n\in \N}
\end{split}
\end{equation*}
Addition and multiplication on $\Q^\N_\textup{Cauchy}$ are both associative and commutative since 
these properties hold componentwise. Furthermore, multiplication distributes from the left and from the 
right over addition, again since this property holds componentwise. 
Next define an embedding $\Q \hookrightarrow \Q^\N_\textup{Cauchy}$  by $r \mapsto (r)_{n\in \N}$,
where $(r)_{n\in \N}$ denotes the constant sequence with each component being equal to $r$.
Obviously,  $\Q \hookrightarrow \Q^\N_\textup{Cauchy}$ preserves the operations of addition and multiplication. 
Moreover, the sequence $(0)_{n\in \N}$ serves as neutral element with respect to addition in  $\Q^\N_\textup{Cauchy}$,
and $(1)_{n\in \N}$ as neutral element with respect to multiplication. Hence $\big( \Q^\N_\textup{Cauchy}, + ,\cdot,(0)_{n\in \N}, (1)_{n\in \N} \big)$ 
is a commutative ring and $\Q \hookrightarrow \Q^\N_\textup{Cauchy}$ an injective ring hommorphism. 
Now we define an equivalence relation $\sim$ on $\Q^\N_\textup{Cauchy}$ as follows. Call two Cauchy sequences 
$(x_n)_{n\in \N}$ and $(y_n)_{n\in \N}$ \emph{equivalent}, in signs $(x_n)_{n\in \N} \sim (y_n)_{n\in \N}$, 
if the sequence $(x_n - y_n)_{n\in \N}$ is a null sequence. Denote the equivalence class of a Cauchy sequence $(x_n)_{n\in \N}$ in $\Q$
by $[(x_n)_{n\in \N}]$, the quotient space $\Q^\N_\textup{Cauchy} \, \big/ \!\sim$ by $\R$, and let $q: \Q^\N_\textup{Cauchy} \to \R$ 
be the quotient map which assigns to every element of $\Q^\N_\textup{Cauchy}$ its equivalence class. 
We will now show that $\sim$ is a congruence relation on
$\Q^\N_\textup{Cauchy}$. This means that for Cauchy sequences $(x_n)_{n\in \N} \sim (x_n')_{n\in \N}$ and $(y_n)_{n\in \N} \sim (y_n')_{n\in \N}$
the two added sequences $(x_n + y_n)_{n\in \N}$ and $(x_n'+y_n')_{n\in \N}$ and the two multiplied sequences 
$(x_n \cdot y_n)_{n\in \N}$ and $(x_n' \cdot y')_{n\in \N}$ are equivalent. 


\begin{lemma}
  The relation $\sim$ is a congruence relation on the commutative ring $\Q^\N_\textup{Cauchy}$. 
\end{lemma}

\begin{proof}
  Asssume that $(x_n)_{n\in \N} \sim (x_n')_{n\in \N}$ and $(y_n)_{n\in \N} \sim (y_n')_{n\in \N}$, 
  where all sequences are Cauchy sequences in $\Q$. Then 
  $(x_n - x_n')_{n\in \N}$ and $(y_n - y_n')_{n\in \N}$ are both null sequences, 
  hence there exist for every $\varepsilon >0$  
  natural numbers $N_1 (\varepsilon)$ and $N_2 (\varepsilon) $ such that  
  $\big| x_n - x_n' \big|  < \varepsilon$ for $n \geq N_1 (\varepsilon)  $ and 
  $\big| y_n - y_n' \big|  < \varepsilon$ for $n \geq N_2 (\varepsilon) $.
  Fix $\varepsilon >0$ and put $N := \max \big\{  N_1 (\frac{\varepsilon}{2}) ,N_2 (\frac{\varepsilon}{2}) \big\} $.
  Then one obtains for all $n \geq N $  
  \begin{equation*}
  \begin{split}
     \big| (x_n + y_n) - (x_n' + y_n') \big| \, & = \big| (x_n - x_n') + (y_n - y_n') \big| \leq  
     \big| x_n - x_n' \big| + \big| y_n - y_n' \big| < \frac{\varepsilon}{2} + \frac{\varepsilon}{2} 
     = \varepsilon \ .
  \end{split}
  \end{equation*}
  Next observe that $(x_n')_{n\in \N}$ and $(y_n)_{n\in \N}$ are bounded since both sequences are Cauchy. Choose $M_1,M_2>0$ such that   
  $|x_n'| \leq M_1$ and $|y_n| \leq M_2$ for all $n \in \N$. Now put 
  $N:= \max \big\{  N_1 (\frac{\varepsilon}{2M_2}) ,N_2 (\frac{\varepsilon}{2M_1}) \big\} $. 
  Then one gets for all  $n \geq N $  
  \begin{equation*}
  \begin{split}
     \big| (x_n \cdot y_n) - (x_n' \cdot y_n') \big| \, & = \big| (x_n - x_n') \cdot y_n + x_n' \cdot (y_n - y_n') \big| \leq  \\
     & \leq \big| x_n - x_n' \big| \, | y_n| + \big| y_n - y_n' \big| \, |x_n'|  < 
     \frac{\varepsilon}{2M_2} \, M_2 + \frac{\varepsilon}{2M_1} \, M_1 = \varepsilon \ .
  \end{split}
  \end{equation*}
  Hence  $(x_n + y_n)_{n\in \N} \sim (x_n'+y'_n)_{n\in \N}$ and $(x_n \cdot y_n)_{n\in \N} \sim (x_n' \cdot y'_n)_{n\in \N}$. 
\end{proof}

As a consequence of the lemma, addition and multiplication on $\Q^\N_\textup{Cauchy}$ descend to unique operations 
$+$ and $\cdot$  on the quotient space $\R$ such that for all rational Cauchy sequences 
$(x_n)_{n\in \N}, (y_n)_{n\in \N}$: 
%\begin{equation}
\begin{align}  
   \label{eq:multiplicativity-quotient-map-cauchy-seqeunces-reals}   
   q \big((x_n)_{n\in \N} + (y_n)_{n\in \N} \big) & = q \big((x_n)_{n\in \N}\big) + q \big((y_n)_{n\in \N}\big)\quad \text{and}\\
   \label{eq:additivity-quotient-map-cauchy-seqeunces-reals}
   q \big((x_n)_{n\in \N} \cdot (y_n)_{n\in \N} \big) & =  q \big((x_n)_{n\in \N}\big) \cdot q \big((y_n)_{n\in \N}\big)
\end{align}
%\end{equation}
The resulting binary operations $+$ and $\cdot$ on $\R$ are both associative and commutative since they are  on $\Q^\N_\textup{Cauchy}$.
Moreover, multiplication on $\R$ distributes over addition, again since $+$ and $\cdot$ have that property on  $\Q^\N_\textup{Cauchy}$.
The mapping $ i : \Q \hookrightarrow \R$, $r \mapsto [(r)_{n\in \N}]$ is injective and preserves addition and multiplication
by Equations \ref{eq:additivity-quotient-map-cauchy-seqeunces-reals} and \ref{eq:multiplicativity-quotient-map-cauchy-seqeunces-reals} 
and since $\Q \to \Q^\N_\textup{Cauchy}$ is a ring homomorphism.
Injectivity follows from the fact that for rational $r, s$ the sequence $(r-s)_{n\in \N}$ is constant, thus a  
null sequence if and only if $r=s$. From now on, we will denote the image of  a rational number $r$ under the embedding $i$ by $r$ as well. 
In particular the elements $[(0)_{n\in \N}]$ and $[(1)_{n\in \N}]$ will be denoted by $0$ and $1$, respectively. 
Last let us define the set $\R^+$ as the set of all equivalence classes of Cauchy sequences $(x_n)_{n\in \N}$ such that 
$ x_n \in \Q^+ = \{ r \in \Q \mid r \geq 0 \}$ for all $n\in \N$. 

\begin{thmanddef}
  The set $\R$ together with addition $+$, multiplication $\cdot$, and the elements $0,1$ becomes a field.
  Moreover, the set $\R^+$ is a positive cone on $\R$. Thus one obtains a total order $\leq$ on $\R$ respecting the monotony laws by 
  defining $x \leq y$  for $x,y\in \R$ if $y-x \in \R^+$. 
  Altogether, $(\R,+,\cdot,0,1,\leq)$ is a complete ordered field called the \emph{field of real numbers}. Finally,
  $i:\Q \hookrightarrow \R$ an embedding of fields. 
\end{thmanddef}

\begin{proof}
  
\end{proof}
